@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

        	<div class="box-header with-border">

        		<h3 class="box-title">Upload Arsip Tambahan
                    <small></small>
                </h3>

        	</div> <!-- /.box-header -->

            <form id="form-simpan" class="form-horizontal" action="{{ route('doUpload-arsip') }}" method="POST" enctype="multipart/form-data">

                <div class="box-body">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="id" value="{{ $arsip->id }}">

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Lokasi file:</label>
                        <div class="col-sm-6">
                            <input type="file" id="file" name="file">
                        </div>   
                    </div> 
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <input type="text" id="keterangan" name="keterangan" class="form-control" maxlength="255" value="{{ old('keterangan')}}">
                        </div>
                    </div>
                    
                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya. File harus dipilih dalam format dokumen pdf maksimal 10 MB.</span> 
                    <div class="pull-right">
                        <a href="{{ route('show-arsip',[encrypt($arsip->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div> <!-- /.box-footer -->

            </form>
            
        </div> <!-- /.box -->      

    </div>

</div>

    
@endsection