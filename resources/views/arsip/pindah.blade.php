@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Rekam Pemindahan Arsip : {{ $arsip->nomor }} ({{($arsip->tanggal == null) ? '' : date('d-m-Y', strtotime($arsip->tanggal)) }})</h3>
            </div>
            <!-- /.box-header -->
            <form id="form-pindah-arsip" class="form-horizontal" action="{{route('doPindah-arsip')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $arsip->id }}">
                <div class="box-body">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Tanggal</label>
                        <div class="col-sm-2">
                          <div class="input-group">
                              <input type="text" class="form-control datepicker text-right" id="tanggal" name="tanggal" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{ old('tanggal') }}">
                              <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                              </div>
                          </div>
                        </div>
                    </div> 
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Lokasi</label>
                        <div class="col-sm-8">
                            <input type="text" id="lokasi" name="lokasi" class="form-control" placeholder="Lokasi pengarsipan" value="{{ old('lokasi') }}" maxlength="255">
                        </div>
                    </div>
                    
                    
                <!-- /.box-body -->
                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a id="link-batal" href="{{ route('show-arsip',[encrypt($arsip->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Simpan</button>
                    </div>

                </div>
            </form>
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->


    </div>

</div>
<script>
    $(document).ready(function () {
               
       $('#form-pindah-arsip').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin akan menyimpan data pemindahan arsip ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
        
    });

</script>

@endsection