@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Arsip Surat                   
                </h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-xs btn-info" href="{{ route('arsip-tahun',[encrypt(date('Y', strtotime($arsip->tanggal)))]) }}" data-toggle="tooltip" title="Kembali ke Daftar Arsip"><i class="fa fa-backward"></i> kembali</a>
                   
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>Ref:</dt>
                    <dd>
                        {{ $arsip->ref }}
                    </dd>
                    <dt>Nomor / Tgl.:</dt>
                    <dd>
                        {{ $arsip->nomor or '' }}
                        @if($arsip->tanggal != null)
                        [{{ date('d-m-Y', strtotime($arsip->tanggal)) }}]
                        @else
                        <i>[Tgl. belum diisi]</i>
                        @endif

                    </dd>
                    <dt>Sifat:</dt>
                    <dd>
                        {{ ucwords($arsip->sifat) }}
                    </dd>
                    <dt>Keamanan:</dt>
                    <dd>
                        {{ ucwords($arsip->keamanan) }}
                    </dd>
                    <dt>Hal:</dt>
                    <dd>{{ $arsip->hal or '' }}</dd>
                    <dt>Tujuan:</dt>
                    <dd>
                        {{ $arsip->tujuan or '' }}
                        @if($arsip->sent_at != null)
                            [dikirim pada {{ date('d-m-Y', strtotime($arsip->sent_at)) }}]
                        @else
                        <i>[belum dikirim]</i>
                        @endif
                    </dd>
                    <dt>Unit Konseptor:</dt>
                    <dd>{{ $arsip->unit->nama or ''}}</dd>
                    <dt>Penandatangan:</dt>
                    <dd>{{ $arsip->pejabat->nama or ''}} ({{ ($arsip->pejabat->jbtn_status != 'definifif') ? $arsip->pejabat->jbtn_status : '' }} {{ $arsip->pejabat->unit->jbtn or ''}})</dd>
                    <dt>Keterangan:</dt>
                    <dd>{{ $arsip->notes or '' }}</dd>
                    <dt>Waktu Disetujui:</dt>
                    <dd>
                        @if($arsip->approved_at == null)
                        <span class="label label-danger" data-toggle="tooltip" title="Belum disetujui"><i class="fa fa-question"></i></span>
                        @else
                        {{ date('d-m-Y H:i:s', strtotime($arsip->approved_at))}}
                        <!--span class="label label-success" data-toggle="tooltip" title="Sudah disetujui"><i class="fa fa-check"></i></span-->

                        @endif
                    </dd>
                                               
                    
                </dl>
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>Riwayat Penerusan:</dt>
                    <dd>
                        <i class="fa fa-square-o"></i> {{ $arsip->creator->nama or '' }} <br />
                        <i><span class="text-green">[dibuat pada {{ date('d-m-Y H:i:s', strtotime($arsip->created_at))}}]</span></i><br />

                        @foreach($arsip->mailSent as $sent)
                        <i class="fa fa-square-o"></i> {{ $sent->userSender->nama or '' }} <i class="fa fa-arrow-right"></i> {{ $sent->userReceiver->nama or '' }} [{{ date('d-m-Y H:i:s', strtotime($sent->created_at))}}]

                        @if($sent->notes != "")
                        - {{ $sent->notes }}
                        @endif
                        <br />
                        @if($sent->read_at == null)
                        <i><span class="text-yellow">[belum dibaca]</span></i>
                        @else
                        <i><span class="text-green">[dibaca pada {{ date('d-m-Y H:i:s', strtotime($sent->read_at)) }}]</span></i>
                        @endif
			<br />
                        @endforeach
                    </dd>

                </dl>
            </div>


            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>File Dukung:</dt>
                    <dd>
                        <?php $i = 1; ?>
                        @foreach($arsip->file as $file)
                            @if($file->tipe == 'pendukung')
				<i class="fa fa-file-pdf-o"></i>
                                    @if($file->active == '1')
                                        Pendukung_{{$i}}.{{ $file->ext }}
                                    @else
                                        <s>Pendukung_{{$i}}.{{ $file->ext }}</s>
                                    @endif
                                
                                @if($file->notes != "")
                                    - {{ $file->notes }}
                                @endif
				&nbsp;&nbsp;&nbsp;
                                    <a data-toggle="tooltip" title="Download" href="{{ route('show-file',[encrypt($file->id)]) }}">
                                        <span class="text-blue"><i class="fa fa-download"></i></span>
                                    </a>
                                <br />
                                <i><small>oleh: {{ $file->creator->nama  or '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($file->created_at))}}</small></i>
                                <br />
                                <?php $i++; ?>
                            @endif
                        @endforeach

                    </dd>
                    <dt></dt>
                    <dd></dd>
                </dl>
            </div>

            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>File Draft / Net:</dt>
                    <dd>
                        @foreach($arsip->file as $file)
                            @if($file->tipe == 'draft' || $file->tipe == 'net')
				@if($file->ext == 'docx' || $file->ext == 'doc')
                                        <i class="fa fa-file-word-o"></i> 
                                    @elseif($file->ext == 'xlsx' || $file->ext == 'xls')
                                        <i class="fa fa-file-excel-o"></i>
                                    @elseif($file->ext == 'pptx' || $file->ext == 'ppt')
                                        <i class="fa fa-file-powerpoint-o"></i>
                                    @else
                                        <i class="fa fa-file-code-o"></i>
                                    @endif 

                                    @if($file->active == '1')
                                        {{ ucwords($file->tipe)}}.{{ $file->ext }}
                                    @else
                                        <s>{{ ucwords($file->tipe)}}.{{ $file->ext }}</s>
                                    @endif
                                @if($file->notes != "")
                                - {{ $file->notes }}
                                @endif
				&nbsp;&nbsp;&nbsp;
                                    <a data-toggle="tooltip" title="Download" href="{{ route('show-file',[encrypt($file->id)]) }}">
                                        <span class="text-blue"><i class="fa fa-download"></i></span>
                                    </a>
                                <br />
                                <i><small>oleh: {{ $file->creator->nama  or '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($file->created_at))}}</small></i>
                                <br />
                            @endif
                        @endforeach

                    </dd>
                    <dt></dt>
                    <dd></dd>
                </dl>
            </div>
            
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>File Arsip:</dt>
                    <dd>
                        @foreach($arsip->file as $file)
                            @if($file->tipe == 'arsip')
					<i class="fa fa-file-pdf-o"></i>
                                    @if($file->active == '1')
                                        Arsip.{{ $file->ext }}
                                    @else
                                        <s>Arsip.{{ $file->ext }}</s>
                                    @endif
                                @if($file->notes != "")
                                    - {{ $file->notes }}
                                @endif
				&nbsp;&nbsp;&nbsp;
                                    <a data-toggle="tooltip" title="Download" href="{{ route('show-file',[encrypt($file->id)]) }}">
                                        <span class="text-blue"><i class="fa fa-download"></i></span>
                                    </a>
                                <br />
                                <i><small>oleh: {{ $file->creator->nama  or '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($file->created_at))}}</small></i>
                                <br />
                             @endif
                        @endforeach
                        <a href="{{ route('upload-arsip', [encrypt($arsip->id)]) }}" class="label label-default" data-toggle="tooltip" title="Tambah upload file arsip"><i class="fa fa-plus"></i> upload file</a>
                    </dd>
                </dl>
            </div>
            
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>Lokasi Arsip:</dt>
                    <dd>
                        <?php $j = 1; ?>
                        @foreach($arsip->mailArsip as $m)
                            {{ ($j > 1) ? 'Dipindahkan ke:' : '' }}
                            {{ $m->lokasi }} <small>[Tgl. {{ date('d-m-Y', strtotime($m->tanggal))}}]</small> <br />
                            <i><small>oleh: {{ ($m->creator) ? $m->creator->nama : '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($m->created_at))}}</small></i><br />
                            <?php $j++; ?>
                        @endforeach
                        <a href="{{ route('pindah-arsip', [encrypt($arsip->id)]) }}" class="label label-default" data-toggle="tooltip" title="Rekam pemindahan arsip"><i class="fa fa-sign-out"></i></a>
                    </dd>
                </dl>
            </div>

            </div> <!-- /. box --> 
        </div>

    </div>

@endsection