@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Arsip Surat Keluar</h3>

                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="arsip">
               <table class="table table-hover">
                    @foreach($data as $t)
                    <tr>
                        <td><a class="black" href="{{ route('arsip-tahun',[encrypt($t['tahun'])]) }}"><i class="fa fa-folder-o fa-2x"> <small>{{ $t['tahun'] }} ({{ $t['sum'] }})</small></i></a></td>
                    <tr/>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /. box -->


    </div>

</div>
@endsection