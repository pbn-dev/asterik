@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Arsip Surat Keluar : Tahun {{ $thn }} <a href='javascript:void(0)' data-toggle="tooltip" data-html="true" title="Arsip hanya dapat dilihat oleh Pegawai dan Plt/Plh. Pejabat Penandatangan pada unit berkenaan.<br />Khusus surat yang bersifat rahasia hanya pejabat unit dan pegawai pembuat surat pada unit berkenaan yang dapat melihat."><i class='fa fa-question-circle'></i></a></h3>

                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="arsip">
               <table class="table table-striped table-hover" id="table">
                   <thead>
                       <tr>
                           <th width="5">#</th>
                           <th>Nomor</th>
                           <th width="10">Tanggal</th>
                           <th>Sifat / Keamanan</th>
                           <th>Hal</th>
                           <th>Tujuan</th>
                           <th>Unit Konseptor</th>
                           <th>Pembuat</th>
                       </tr>
                   </thead>
                    <?php $i = 1; ?>
                    @foreach($arsip as $t)
                    <tr>
                        <td>{{ $i }}</td>
                        <td><a href="{{ route('show-arsip',[encrypt($t->id)]) }}">{{ $t->nomor }}</a></td>
                        <td>{{ date('d-m-Y', strtotime($t->tanggal)) }}</td>
                        <td>{{ ucwords($t->sifat) }}{{ ($t->keamanan == 'rahasia') ? ' / '.ucwords($t->keamanan) : '' }}</td>
                        <td>{{ $t->hal }}</td>
                        <td>{{ $t->tujuan }}</td>
                        <td>{{ $t->unit->nama or '' }}</td>
                        <td>{{ $t->creator->nama or '' }}</td>
                    <?php $i++; ?>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /. box -->


    </div>

</div>
<script type="text/javascript">
    
    $('#table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
</script>
@endsection