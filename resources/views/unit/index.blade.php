@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Daftar Unit
                    <small></small>
                </h3>
                
                <div class="box-tools pull-right">
                    <a class="btn btn-xs btn-success" href="{{ route('rekam-unit') }}" data-toggle="tooltip" title="Tambah Unit"><i class="fa fa-plus"></i></a>                 
                </div>
            </div> <!-- /.box-header -->

            <div class="box-body">

                <table class="table table-bordered table-hover">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Unit</th>
                            <th>Jabatan</th>
                            <th>Jabatan Singkat</th>
                            <th>Pejabat</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($unit as $u)
                        <tr>
                            <td>                                
                                @if(strlen($u->id) == 6)
                                 &nbsp;&nbsp;
                                @endif
                                {{ $u->id }}
                            </td>
                            <td>{{ $u->nama }}</td>
                            <td>{{ $u->jbtn }}</td>
                            <td>{{ $u->jbtn_short }}</td>
                            <td>
                                @foreach($u->pejabat as $p)
                                    {{ $p->nama .' NIP'.$p->nip }}
                                    @if($p->jbtn_status != 'definitif')
                                    ({{$p->jbtn_status}})
                                    @endif
                                    <br />
                                @endforeach
                            </td>
                            <td>
                                @if($u->active == '1')
                                <a class="label label-warning" href="{{ route('edit-unit',[encrypt($u->id)]) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('change-status-unit',[encrypt($u->id)]) }}" class="link-update-status" data-toggle="tooltip" title="Ubah Status">
                                <span class="label label-success">Aktif</span>
                                </a>
                                @else
                                <a href="{{ route('change-status-unit',[encrypt($u->id)]) }}" class="link-update-status" data-toggle="tooltip" title="Ubah Status">
                                <span class="label label-danger">Inaktif</span>
                                </a>
                                @endif
                                
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>

                </table>


            </div> <!-- /.box-body -->


        </div> <!-- /.box -->      

    </div>

</div>

<script>
    $(document).ready(function () {
        $('.link-update-status').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin ubah status unit ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });
    })

</script>

@endsection