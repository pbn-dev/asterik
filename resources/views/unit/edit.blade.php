@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Edit Unit
                    <small></small>
                </h3>

            </div> <!-- /.box-header -->

            <form id="form-simpan" class="form-horizontal" action="{{ route('doEdit-unit') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="box-body">

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">ID</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input class="form-control" type="text" value="{{ $unit->id }}" name="id" id="id" placeholder="ID" Readonly="Readonly">
                            </div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Nama Unit</label>
                        <div class="col-sm-4">
                            <input type="text" name="nama" class="form-control" id="nama" value="{{ $unit->nama }}">
                        </div>
                    </div>
                    
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Nama Jabatan</label>
                        <div class="col-sm-4">
                            <input type="text" name="jbtn" class="form-control" id="jbtn" value="{{ $unit->jbtn }}">
                        </div>
                    </div>
                    
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Singkatan Nama Jabatan (Singkatan)</label>
                        <div class="col-sm-4">
                            <input type="text" name="jbtn_short" class="form-control" id="jbtn_short" value="{{ $unit->jbtn_short }}">
                        </div>
                    </div>

                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a href="{{ route('unit') }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <br /><br />
                    <span class="pull-left">
                        
                        Pengisian ID unit agar diperhatikan sebagai berikut: <br /><br />
                        <strong>Untuk unit kerja setingkat unit eselon III, maka 2 digit untuk unit eselon III dan 4 digit untuk unit eselon IV.</strong><br />
                        
                        Contoh pengisian: <br />
                        10 - Kantor Pelayanan Perbendaharaan Negara Pelaihari<br />
                        1010 - Subbagian Umum --><span class="red">2 digit pertama harus inline dengan ID unit eselon III-nya</span><br />
                        1020 - Seksi Pencairan Dana<br />
                        <br /><br />
                        
                        <strong>Untuk unit kerja setingkat unit eselon II, maka 2 digit untuk unit eselon II, 4 digit untuk unit eselon III, dan 16 digit untuk unit eselon IV.</strong><br />
                        Contoh pengisian: <br />
                        10 - Kantor Wilayah Direktorat Jenderal Perbendaharaan Negara Kalimantan Selatan<br />
                        1011 - Bagian Umum --> <span class="red">2 digit pertama harus inline dengan ID unit eselon II-nya</span><br />
                        101101 - Subbagian Kepegawaian --> <span class="red">4 digit pertama harus inline dengan ID unit eselon III-nya</span><br />
                        101102 - Subbagian Tata Usaha dan Rumah Tangga<br />
                        1012 - Bidang Pembinaan Pelaksanaan Anggaran I<br />
                        101201 - Seksi Pembinaan Pelaksanaan Anggaran I A<br />
                        101202 - Seksi Pembinaan Pelaksanaan Anggaran I B<br />
                        101203 - Seksi Pembinaan Pelaksanaan Anggaran I C<br />
                    </span> 
                </div> <!-- /.box-footer -->

            </form>
            <div>
                <table class="table table-bordered table-hover">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Unit</th>
                            <th>Jabatan</th>
                            <th>Jabatan Singkat</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($units as $u)
                        <tr>
                            <td>                                
                                @if(strlen($u->id) == 6)
                                 &nbsp;&nbsp;
                                @endif
                                {{ $u->id }}
                            </td>
                            <td>{{ $u->nama }}</td>
                            <td>{{ $u->jbtn }}</td>
                            <td>{{ $u->jbtn_short }}</td>
                            <td>
                                @if($u->active == '1')
                                <span class="label label-success">Aktif</span>
                                @else
                                <span class="label label-danger">Inaktif</span>
                                @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div> <!-- /.box -->      
        
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#role").select2();
        $("#unit").select2();
        $("#sama-nip").click(function(){
            $("#username").val($("#nip").val());
        });

        
    });
</script>

@endsection