@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Daftar Pejabat Penandatangan
                    <small></small>
                </h3>

                <div class="box-tools pull-right">
                    <a class="btn btn-xs btn-success" href="{{ route('rekam-pejabat') }}" data-toggle="tooltip" title="Tambah Pejabat"><i class="fa fa-plus"></i></a>                 
                </div>

            </div> <!-- /.box-header -->

            <div class="box-body">

                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center vertical-center" width="4%">No</th>
                            <th class="text-center vertical-center">NIP</th>
                            <th class="text-center vertical-center">Nama</th>
                            <th class="text-center vertical-center">Jabatan</th>
                            <th class="text-center vertical-center">Status Jabatan</th>
                            <th class="text-center vertical-center">Action</th>

                    </thead>

                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($pejabat as $v)
                        <tr>
                            <td class="text-center">{{ $i }} </td>
                            <td>{{ $v->nip }}</td>
                            <td>{{ $v->nama }} </td>
                            <td class="text-left">{{ $v->unit->jbtn or '' }}</td>
                            <td class="text-left">{{ $v->jbtn_status or '' }}</td>
                            <td class="text-center">
                                <a class="label label-warning" href="{{ route('edit-pejabat',[encrypt($v->id)]) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                <a class="label label-danger link-delete-pejabat" href="{{ route('change-status-pejabat',[encrypt($v->id)]) }}" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></a>  
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>


            </div> <!-- /.box-body -->


        </div> <!-- /.box -->      

    </div>

</div>
<script type="text/javascript">

    $(function () {

        $('.link-update-status').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin ubah status pejabat ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });


        $('.link-delete-pejabat').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin menghapus pejabat ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });
        
        
    });

</script>


@endsection