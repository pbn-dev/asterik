@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Edit Pejabat
                    <small></small>
                </h3>

            </div> <!-- /.box-header -->

            <form id="form-simpan" class="form-horizontal" action="{{ route('doEdit-pejabat') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $pejabat->id }}">

                <div class="box-body">

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">NIP/Nama</label>
                        <div class="col-sm-2">
                            <input value="{{ $pejabat->nip or ''}}" class="form-control" type="text" name="nip" id="nip" maxlength="18" placeholder="NIP" value="{{ old('nip') }}">
                        </div>
                        <div class="col-sm-5">
                            <input value="{{ $pejabat->nama or '' }}" class="form-control" type="text" name="nama" id="nama" value="{{ old('nama') }}" placeholder="nama">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Jabatan</label>
                        <div class="col-sm-5">
                            <select name="jabatan" id="jabatan" class="form-control">
                                @foreach ($unit as $uk)
                                <option value="{{ $uk->id }}" {{ ($uk->id == $pejabat->unit_id) ? 'selected' : '' }}>
                                    {{ $uk->jbtn }} 
                                </option>
                                @endforeach                                    
                            </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Status Jabatan</label>
                        <div class="col-sm-3">
                            <select name="status_jabatan" id="status_jabatan" class="form-control">
                                <option value="definitif" {{ ($pejabat->jbtn_status == 'definitif') ? 'selected': ''  }}>Definitif</option>
                                 <option value="plt." {{ ($pejabat->jbtn_status == 'plt.') ? 'selected': ''  }}>Pelaksana Tugas</option>  
                                 <option value="plh." {{ ($pejabat->jbtn_status == 'plh.') ? 'selected': ''  }}>Pelaksana Harian</option> 
                            </select>
                        </div>
                    </div>

                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a href="{{ route('pejabat') }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div> <!-- /.box-footer -->

            </form>

        </div> <!-- /.box -->      

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#role").select2();
        $("#unit").select2();
        $("#sama-nip").click(function(){
            $("#username").val($("#nip").val());
        });

//        $("#nip").keyup(function() {
//            var url = "";
//            $("#nip").autocomplete({
//                minLength:4,
//                source:url,
//                select:function(event, ui){
//                  $('#nama').val(ui.item.nama);
//                },
//                change: function( event, ui ) {
//                    if(ui.item==null) 
//                    {
//                    this.value='';
//                      $('#nama').val('');
//                    
//                },
//                response:function(event, ui){
//                  if(!ui.content.length){
//                    var noresult = {value:"", label:"no result found", nama:""};
//                    ui.content.push(noresult);
//                  }
//                },
//                error:function(jqXHR,textStatus,errorThrown){
//                    var noresult = {value:"", label:"error", nama:""};
//                    ui.content.push(noresult);
//                    console.log(error);
//                }
//              });
//        });


    });
</script>

@endsection