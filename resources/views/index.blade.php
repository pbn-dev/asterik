@extends('layout.master')

@section('content-header')
<h1>
    Beranda
    <small>
        Saya
        @if(count($pejabat) > 0)
        (
        @endif
        
        @php ($i=1)
        @foreach($pejabat as $p)        
            @if($p->jbtn_status != "definitif")
            {{ strtoupper($p->jbtn_status) }}
            @endif
            {{ $p->unit->jbtn_short }}
            @if($i < count($pejabat))
            ;
            @endif
            @php ($i++)
        @endforeach
        @if(count($pejabat) > 0)
        )
        @endif
    </small>
</h1>
@stop

@section('content')
<div id="dashboard">
    
</div>

<script type="text/javascript">
    
    $(document).ready(function () {
    
        $('#dashboard').load("{{ route('dashboard') }}");
        setInterval(function(){
            $('#dashboard').load("{{ route('dashboard') }}");
        },5000);
        
    });
    
</script>
@endsection