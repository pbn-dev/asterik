@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

        	<div class="box-header with-border">

        		<h3 class="box-title">Daftar Log User
                    <small></small>
                </h3>

                
        	</div> <!-- /.box-header -->

            <div class="box-body">

                <table id="datatables" class="table table-bordered table-hover">

                    <thead>
                        <tr>
                            <th width="5%">No.</th>
                            <th width="15%">NIP</th>
                            <th width="30%">Nama</th>
                            <th width="15%">Status</th>
                            <th width="15%">IP</th>
                            <th width="20%">Waktu</th>
                        </tr>
                    </thead>

                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($data as $u)
                        <tr>
                            <td>{{ $i }}.</td>
                            <td>{{ $u->nip }}</td>
                            <td>{{ $u->user->nama or '' }}</td>
                            <td>
                                @if($u->success == '1')
                                    <span class="label label-success">Sukses</span>
                                    @else
                                    <span class="label label-danger">Gagal</span>
                                    @endif
                            </td>
                            <td>{{ $u->ip }}</td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($u->created_at)) }}</td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                    </tbody>

                </table>

                
                
            </div> <!-- /.box-body -->

            
        </div> <!-- /.box -->      

    </div>

</div>
<script type="text/javascript">

  $(function () {
    $('#datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

</script>

    
@endsection