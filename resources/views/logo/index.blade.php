@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

        	<div class="box-header with-border">

        		<h3 class="box-title">Logo dan Icon
                    <small></small>
                </h3>

        	</div> <!-- /.box-header -->

            

                <div class="box-body">
                    
                    <form id="form-upload-logo" class="form-horizontal" action="{{ route('upload-logo') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Logo:</label>
                        <div class="col-sm-6">
                            <img id="logo" src="" style="max-width:200px;max-height:80px"/>
                            <input type="file" id="file" name="file">
                            <span class="pull-left"><span class="red">*</span> File harus dipilih dalam format gambar (png) maksimal 5 MB</span>
                            &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info btn-xs"><i class="fa fa-save"></i> Upload</button>
                            
                        </div> 
                    </div> 
                    </form>
                    
                    <form id="form-upload-favicon" class="form-horizontal" action="{{ route('upload-favicon') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Favicon:</label>
                        <div class="col-sm-6">
                            <img id="favicon" src=""/>
                            <input type="file" id="file" name="file">
                            <span class="pull-left"><span class="red">*</span> File harus dipilih dalam format gambar (ico) maksimal 1 MB</span>
                            &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info btn-xs"><i class="fa fa-save"></i> Upload</button>
                            
                            
                        </div> 
                    </div> 
                    
                    </form>

                    
                    
                </div> <!-- /.box-body -->

                

            
        </div> <!-- /.box -->      

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        
        $("#logo").attr("src", "{{ asset('/img/logo.png') }}");
        $("#favicon").attr("src", "{{ asset('/img/favicon.ico') }}");
            
        $('#form-upload-logo').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin upload file logo ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
        $('#form-upload-favicon').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin upload file favicon ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
        
    });
</script>
@endsection