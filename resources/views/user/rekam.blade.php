@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Rekam User
                    <small></small>
                </h3>

            </div> <!-- /.box-header -->

            <form id="form-simpan" class="form-horizontal" action="{{ route('doRekam-user') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="box-body">

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">NIP/Nama</label>
                        <div class="col-sm-2">
                            <input class="form-control" type="text" name="nip" id="nip" maxlength="18" placeholder="NIP" value="{{ old('nip') }}">
                        </div>
                        <div class="col-sm-5">
                            <input class="form-control" type="text" name="nama" id="nama" value="{{ old('nama') }}" placeholder="nama">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input class="form-control" type="text" value="{{ old('username') }}" name="username" id="username" placeholder="Username">
                                <div class="input-group-addon">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Sama dengan nip" id="sama-nip"><i class="fa fa-copy"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-4">
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Konfirmasi Password</label>
                        <div class="col-sm-4">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="email" id="email" placeholder="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Unit</label>
                        <div class="col-sm-5">
                            <select name="unit" id="unit" class="form-control">
                                @foreach ($unit as $uk)
                                <option value="{{ $uk->id }}">
                                    {{ $uk->nama }} 
                                </option>
                                @endforeach                                    
                            </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Role</label>
                        <div class="col-sm-3">
                            <select name="role" id="role" class="form-control">
                                @foreach ($role as $rl)
                                <option value="{{ $rl->id }}">
                                    {{ title_case($rl->nama) }}
                                </option>
                                @endforeach                                    
                            </select>
                        </div>
                    </div>

                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a href="{{ route('user') }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div> <!-- /.box-footer -->

            </form>

        </div> <!-- /.box -->      

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#role").select2();
        $("#unit").select2();
        $("#sama-nip").click(function(){
            $("#username").val($("#nip").val());
        });

        
    });
</script>

@endsection