@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

        	<div class="box-header with-border">

        		<h3 class="box-title">Profile
                    <small></small>
                </h3>

                <div class="box-tools pull-right">
                        <a class="btn btn-xs btn-success" href="{{ route('change-password') }}" data-toggle="tooltip" title="Ganti Password"><i class="fa fa-key"></i> Ganti Password</a>                 
                </div>

        	</div> <!-- /.box-header -->

            <form id="form-simpan" class="form-horizontal" action="{{ route('doProfile') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="box-body">

                        <div class="form-group required">
                          <label class="col-sm-2 control-label">NIP/Nama</label>
                          <div class="col-sm-2">
                            <input class="form-control" type="text" name="nip" id="nip" maxlength="18" placeholder="NIP" value="{{ $user->nip }}" readonly="readonly">
                          </div>
                           <div class="col-sm-5">
                            <input class="form-control" type="text" name="nama" id="nama" value="{{ $user->nama }}" placeholder="nama">
                          </div>
                        </div>
                        
                        <div class="form-group required">
                          <label class="col-sm-2 control-label">Username</label>
                          <div class="col-sm-4">
                            <input class="form-control" type="text" name="username" id="username" placeholder="username" value="{{ $user->username }}" readonly="readonly">
                           </div>
                        </div>


                        <div class="form-group required">
                          <label class="col-sm-2 control-label">Email</label>
                          <div class="col-sm-4">
                            <input class="form-control" type="text" name="email" id="email" placeholder="email" value="{{ $user->email }}">
                           </div>
                        </div>
                    
                        <div class="form-group required">
                          <label class="col-sm-2 control-label">Unit</label>
                          <div class="col-sm-4">
                            <input class="form-control" type="text" name="unit" id="username" placeholder="unit" value="{{ $user->unit->nama or '' }}" readonly="readonly">
                           </div>
                        </div>
                    
                        <div class="form-group required">
                          <label class="col-sm-2 control-label">Role</label>
                          <div class="col-sm-4">
                            <input class="form-control" type="text" name="unit" id="username" placeholder="unit" value="{{ $user->role->nama or '' }}" readonly="readonly">
                           </div>
                        </div>

                                                                          
                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <!--button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Batal</button-->
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div> <!-- /.box-footer -->

            </form>

        </div> <!-- /.box -->      

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {

       
      

    });
</script>
    
@endsection