@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Daftar User
                    <small></small>
                </h3>

                <div class="box-tools pull-right">
                    <a class="btn btn-xs btn-success" href="{{ route('rekam-user') }}" data-toggle="tooltip" title="Tambah User"><i class="fa fa-plus"></i></a>                 
                </div>

            </div> <!-- /.box-header -->

            <div class="box-body">

                <table id="tabels" class="table table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th class="text-left">No.</th>
                            <th class="text-left">Username</th>
                            <th class="text-left">NIP</th>
                            <th class="text-left">Nama</th>
                            <th class="text-left">Unit</th>
                            <th class="text-left">Role</th>
                            <th class="text-left">Email</th>
                            <!--th class="text-left">Tgl. Dibuat</th--> 
                            <th class="text-left">Status</th>
                            <th class="text-left">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($user as $v)
                        <tr>
                            <td class="text-left">{{ $i }} </td>
                            <td class="text-left">{{ $v->username }}</td>
                            <td class="text-left">{{ $v->nip }}</td>
                            <td class="text-left">{{ $v->nama }} </td>
                            <td class="text-left">{{ $v->unit->nama or '' }}</td>
                            <td class="text-left">{{ $v->role->nama or '' }}</td>
                            <td class="text-left">{{ $v->email }}</td>
                            <!--td class="text-left">{{ date('d-m-Y H:i:s', strtotime($v->created_at)) }}</td-->
                            <td class="text-left"><a href="{{ route('change-status-user',[encrypt($v->nip)]) }}" class="link-update-status">
                                    @if($v->active == '1')
                                    <span class="label label-success" data-toggle="tooltip" title="Klik untuk mengubah status">Aktif</span>
                                    @else
                                    <span class="label label-danger" data-toggle="tooltip" title="Klik untuk mengubah status">Inaktif</span>
                                    @endif
                                </a>
                            </td>
                            <td class="text-left">
                                <a class="label label-warning" href="{{ route('edit-user',[encrypt($v->nip)]) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                <a class="label label-danger link-delete-user" href="{{ route('delete-user',[encrypt($v->nip)]) }}" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></a>  
                                <a class="label label-info link-reset-user" href="{{ route('resetPass-user',[encrypt($v->nip)]) }}" data-toggle="tooltip" title="Reset Password"><i class="fa fa-key"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>


            </div> <!-- /.box-body -->


        </div> <!-- /.box -->      

    </div>

</div>
<script type="text/javascript">

    $(function () {

        $('#tabel').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
            
        });

        $('.link-update-status').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin ubah status user ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });


        $('.link-delete-user').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin menghapus user ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });

        $('.link-reset-user').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            bootbox.confirm("Yakin mereset password user ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });

    });

</script>


@endsection