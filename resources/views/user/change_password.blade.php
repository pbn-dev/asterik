@extends('layout.master')

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">

      <div class="box-header with-border">

            <h3 class="box-title">Ganti Password
                    <small></small>
                </h3>

      </div> <!-- /.box-header -->

			<form id="form-simpan" class="form-horizontal" action="{{ route('doChange-password') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                 <div class="form-group required">
                  <label class="col-sm-3 control-label">Password Lama</label>
                  <div class="col-sm-3">
                    <input type="password" name="password_old" class="form-control" id="password_old" value="{{ $password_old }}">
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-3 control-label">Password Baru</label>
                  <div class="col-sm-3">
                    <input type="password" name="password" class="form-control" id="password">
                  </div>
                </div>
                 <div class="form-group required">
                  <label class="col-sm-3 control-label">Konfirmasi Password Baru</label>
                  <div class="col-sm-3">
                     <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                  </div>
                </div>
               
               
              </div-->
              <!-- /.box-body -->
              <div class="box-footer">
              	 <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                <div class="pull-right">
                  <a href="{{ route('profile') }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
                
              </div>
              <!-- /.box-footer -->
            <!--/form-->	
		</div>
	</div>
</div>

@stop