@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Draft / Net Surat    
                    
                    <?php
                    $cls1 = "disabled";
                    if ($draft->approved_at == null) {
                        $cls1 = "";
                    }
                    
                    $cls2 = "disabled";
                    if ($draft->pejabat->nip == Auth::user()->nip && $draft->sent_at == null) {
                        $cls2 = "";
                    }
                    
                    $cls = "disabled";
                    if ($draft->latestMailSent == "") {
                        if ($draft->created_by == Auth::user()->nip) {  // if mail created by current user
                            $cls = "";
                        }
                    } else {
                        if ($draft->latestMailSent->receiver == Auth::user()->nip || $draft->latestMailSent->sender == Auth::user()->nip) { // if last forwarded mail to current user
                            $cls = "";
                        }
                    }
                    ?>
                    
                    <a class="label label-warning {{ $cls1 }}" href="{{ route('edit-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Edit Draft/Net"><i class="fa fa-pencil"></i></a>
                    <a class="label label-danger link-remove-draft {{ $cls1 }}" href="{{ route('remove-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Hapus Draft/Net"><i class="fa fa-trash"></i></a>
                    
                    
                </h3>
                <div class="box-tools pull-right">
                    
                    <a class="btn btn-xs btn-default" href="{{ route('draft') }}" data-toggle="tooltip" title="Kembali ke Daftar Draft / Net"><i class="fa fa-backward"></i> kembali</a>

                    <a class="btn btn-xs btn-default {{ $cls }}" href="{{ route('teruskan-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Teruskan Draft / Net"><i class="fa fa-mail-forward"></i> Teruskan</a>
                    @if($draft->approved_at == null)
                        <a class="btn btn-xs btn-default link-acc {{ $cls2 }}" href="{{ route('approved-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Setujui Draft/Net"><i class="fa fa-thumbs-o-up"></i> Setuju</a>
                    @else
                        <a class="btn btn-xs btn-default link-unacc {{ $cls2 }}" href="{{ route('disApproved-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Batal Setujui Draft/Net"><i class="fa fa-thumbs-o-down"></i> Tolak</a>
                        <a class="btn btn-xs btn-default link-net" href="{{ route('rekam-tanggal',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Rekam Tanggal Draft/Net"><i class="fa fa-calendar-check-o"></i> Rekam Tanggal</a>
                        <a class="btn btn-xs btn-default" href="{{ route('kirim-mail',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Kirim ke tujuan"><i class="fa fa-send-o"></i> Rekam Pengiriman</a>
                    @endif
                    @if($draft->sent_at != null)
                        <a class="btn btn-xs btn-default" href="{{ route('arsip-mail',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Arsipkan surat"><i class="fa fa-archive"></i> Rekam Pengarsipan</a>
                    @endif

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt>Ref:</dt>
                    <dd>
                        {{ $draft->ref }}
                    </dd>
                    <dt>Nomor / Tanggal:</dt>
                    <dd>
                        {{ $draft->nomor or '' }}
                        @if($draft->tanggal != null)
                        [{{ date('d-m-Y', strtotime($draft->tanggal)) }}]
                        @else
                        <i>[Tgl. belum diisi]</i>
                        @endif

                    </dd>
                    <dt>Sifat:</dt>
                    <dd>
                        {{ ucwords($draft->sifat) }}
                    </dd>
                    <dt>Keamanan:</dt>
                    <dd>
                        {{ ucwords($draft->keamanan) }}
                    </dd>
                    <dt>Hal:</dt>
                    <dd>{{ $draft->hal or '' }}</dd>
                    <dt>Tujuan:</dt>
                    <dd>
                        {{ $draft->tujuan or '' }}
                        @if($draft->sent_at != null)
                            [dikirim pada {{ date('d-m-Y', strtotime($draft->sent_at)) }}]
                        @else
                        <i>[Belum dikirim]</i>
                        @endif
                    </dd>
                    <dt>Unit Konseptor:</dt>
                    <dd>{{ $draft->unit->nama or ''}}</dd>
                    <dt>Penandatangan:</dt>
                    <dd>{{ $draft->pejabat->nama or ''}} ({{ ($draft->pejabat->jbtn_status != 'definifif') ? $draft->pejabat->jbtn_status : '' }} {{ $draft->pejabat->unit->jbtn or ''}})</dd>
                    <dt>Keterangan:</dt>
                    <dd>{{ $draft->notes or '' }}</dd>
                    <dt>Waktu Disetujui:</dt>
                    <dd>
                        @if($draft->approved_at == null)
                        <i>[Belum disetujui]</i>
                        @else
                        {{ date('d-m-Y H:i:s', strtotime($draft->approved_at))}}
                        <!--span class="label label-success" data-toggle="tooltip" title="Sudah disetujui"><i class="fa fa-check"></i></span-->

                        @endif
                    </dd>
                                       
                    
                </dl>
            </div>
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Riwayat Penerusan:</dt>
                        <dd>
                            <i class="fa fa-square-o"></i> {{ $draft->creator->nama or '' }} <br />
                            <i><span class="text-green">[dibuat pada {{ date('d-m-Y H:i:s', strtotime($draft->created_at))}}]</span></i>
                            @if(count($draft->mailSent) < 1)
                                <i><span class="text-yellow">[belum diteruskan]</span></i>
                            @endif
                            <br />
                            
                            @foreach($draft->mailSent as $sent)
                            <i class="fa fa-square-o"></i> {{ $sent->userSender->nama or '' }} <i class="fa fa-arrow-right"></i> {{ $sent->userReceiver->nama or '' }} [{{ date('d-m-Y H:i:s', strtotime($sent->created_at))}}]

                            @if($sent->notes != "")
                            - {{ $sent->notes }}
                            @endif
                            <br />
                            @if($sent->read_at == null)
                            <i><span class="text-yellow">[belum dibaca]</span></i>
                            @else
                            <i><span class="text-green">[dibaca pada {{ date('d-m-Y H:i:s', strtotime($sent->read_at)) }}]</span></i>
                            @endif

                            @if($sent->sender == Auth::user()->nip AND $sent->read_at == null AND $draft->approved_at == null)
                            <a class="link-remove-mailsent" href="{{ route('remove-mailsent',[encrypt($sent->id)]) }}"><span class="label label-danger"><i class="fa fa-remove"></i></span></a>
                            @endif
                            <br />
                            @endforeach
                         
                        </dd>

                    </dl>
                </div>


                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>File Dukung:</dt>
                        <dd>
                            <?php $i = 1; ?>
                            @foreach($draft->file as $file)
                                @if($file->tipe == 'pendukung')
                                    
                                        <i class="fa fa-file-pdf-o"></i>
                                        @if($file->active == '1')
                                            Pendukung_{{$i}}.{{$file->ext}}
                                        @else
                                            <s>Pendukung_{{$i}}.{{$file->ext}}</s>
                                        @endif
                                    
                                    @if($file->notes != "")
                                        - {{ $file->notes }}
                                    @endif
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="{{ route('show-file',[encrypt($file->id)]) }}" target="_BLANK">
                                        <span class="text-blue"><i class="fa fa-download"></i></span>
                                    </a>
                                    @if($file->created_by == Auth::user()->nip && $file->active == '1' AND $draft->approved_at == null)
                                        <a class="link-remove-file-pendukung" href="{{ route('remove-file',[encrypt($file->id)]) }}" data-toggle="tooltip" title="Hapus"><span class="label text-red"><i class="fa fa-remove"></i></span></a>
                                    @endif
                                    <br />
                                    <i><small>Diupload oleh: {{ $file->creator->nama  or '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($file->created_at))}}</small></i>
                                    <br />
                                    <?php $i++; ?>
                                @endif
                            @endforeach
                            <a class="label label-default" href="{{ route('upload-pendukung',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Tambah upload file pendukung"><i class="fa fa-plus"></i> upload file</a>
                        </dd>
                        <dt></dt>
                        <dd></dd>
                    </dl>
                </div>

                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>File Draft / Net:</dt>
                        <dd>
                            @foreach($draft->file as $file)
                                @if($file->tipe == 'draft' || $file->tipe == 'net')
                                
                                    @if($file->ext == 'docx' || $file->ext == 'doc')
                                        <i class="fa fa-file-word-o"></i> 
                                    @elseif($file->ext == 'xlsx' || $file->ext == 'xls')
                                        <i class="fa fa-file-excel-o"></i>
                                    @elseif($file->ext == 'pptx' || $file->ext == 'ppt')
                                        <i class="fa fa-file-powerpoint-o"></i>
                                    @else
                                        <i class="fa fa-file-code-o"></i>
                                    @endif
                                    
                                    @if($file->active == '1')
                                        {{ ucwords($file->tipe)}}.{{ $file->ext }}
                                    @else
                                        <s>{{ ucwords($file->tipe)}}.{{ $file->ext }}</s>
                                    @endif
      
                                    @if($file->notes != "")
                                    - {{ $file->notes }}
                                    @endif
                                    &nbsp;&nbsp;&nbsp;
                                    <a data-toggle="tooltip" title="Download" href="{{ route('show-file',[encrypt($file->id)]) }}">
                                        <span class="text-blue"><i class="fa fa-download"></i></span>
                                    </a>
                                    @if($file->created_by == Auth::user()->nip && $file->active == '1' AND $draft->approved_at == null)
                                        <a class="link-remove-file-draft" href="{{ route('remove-file',[encrypt($file->id)]) }}" data-toggle="tooltip" title="Hapus"><span class="text-red" ><i class="fa fa-remove"></i></span></a>
                                    @endif
                                    
                                    @if($draft->approved_at == null AND $file->active == '1' AND $file->tipe == 'draft' AND ($draft->created_by == Auth::user()->nip || $draft->pejabat->nip == Auth::user()->nip))
                                        <a class="link-change-tipe-file" href="{{ route('change-tipe-file',[encrypt($file->id)]) }}" data-toggle="tooltip" title="Pilih Net"><span class="text-green"><i class="fa fa-check"></i></span></a>
                                    @endif
                                    
                                    <br />
                                    <i><small>Diupload oleh: {{ $file->creator->nama  or '' }}, pada: {{ date('d-m-Y H:i:s', strtotime($file->created_at))}}</small></i>
                                    <br />
                                @endif
                            @endforeach
                            <a class="label label-default" href="{{ route('upload-draft',[encrypt($draft->id)]) }}" data-toggle="tooltip" title="Tambah upload file draft / net"><i class="fa fa-plus"></i> upload file</a>
                        </dd>
                        <dt></dt>
                        <dd></dd>
                    </dl>
                </div>

            </div> <!-- /. box --> 
        </div>

    </div>
<script>
     $('.link-remove-draft').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan menghapus draft / net ini ?", function (confirmation) {
            bootbox.confirm("Yakin akan menghapus draft / net ?", function (confirmation) {
                confirmation && document.location.assign($link.attr('href'));
            });
        });
    });
    
    $('.link-remove-mailsent').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan batalkan penerusan draft / net ?", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });
    
    $('.link-remove-file-pendukung').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan menghapus file pendukung ?", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });
    
    $('.link-remove-file-draft').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan menghapus file draft / net ?", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });

    $('.link-acc').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan menyetujui draft / net ini ?", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });
    
    $('.link-change-tipe-file').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin pilih draft ini menjadi net ?", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });

    $('.link-unacc').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        bootbox.confirm("Yakin akan membatalkan persetujuan draft / net ini ? Tanggal dan Pengiriman akan direset kembali.", function (confirmation) {
            confirmation && document.location.assign($link.attr('href'));
        });
    });
    
    

</script>

@endsection