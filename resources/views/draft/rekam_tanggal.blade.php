@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Rekam Tanggal Draft / Net</h3>
            </div>
            <!-- /.box-header -->
            <form id="form-simpan-tanggal" class="form-horizontal" action="{{route('doRekam-tanggal')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $draft->id }}">
                <div class="box-body">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Tanggal</label>
                        <div class="col-sm-2">
                          <div class="input-group">
                              <input type="text" class="form-control datepicker text-right" id="tanggal" name="tanggal" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{ ($draft->tanggal == null) ? '' : date('d-m-Y', strtotime($draft->tanggal)) }}">
                              <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                              </div>
                          </div>
                        </div>
                    </div> 
                </div>    
                <!-- /.box-body -->
                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a href="{{ route('show-draft',[encrypt($draft->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Simpan</button>
                    </div>

                </div>
            </form>
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->


    </div>

</div>
<script>
    $(document).ready(function () {
        $("#sifat").select2();
        $("#unit_konseptor").select2();
        $("#pejabat").select2();
        
        $('#form-simpan-tanggal').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin simpan data tanggal draft/net?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
    });

</script>

@endsection