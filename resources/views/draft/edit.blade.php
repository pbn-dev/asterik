@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Draft / Net</h3>
            </div>
            <!-- /.box-header -->
            <form id="form-simpan-draft" class="form-horizontal" action="{{route('update-draft')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $draft->id }}">
                <div class="box-body">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Nomor</label>
                        <div class="col-sm-4">
                            <input class="form-control" name="nomor" id="nomor" placeholder="contoh: S-    /WPB.01/KP.01/2017" value="{{ $draft->nomor }}">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Hal</label>
                        <div class="col-sm-8">
                            <textarea id="hal" name="hal" class="form-control" style="height: 50px" placeholder="perihal surat">{{ $draft->hal }}</textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Sifat</label>
                        <div class="col-sm-2 required">
                            <select class="form-control" name="sifat" id="sifat">
                                <option value="biasa" {{ ($draft->sifat == 'biasa') ? 'selected' : '' }}>Biasa</option>
                                <option value="segera" {{ ($draft->sifat == 'segera') ? 'selected' : '' }}>Segera</option>
                                <option value="sangat segera" {{ ($draft->sifat == 'sangat segera') ? 'selected' : '' }}>Sangat Segera</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keamanan</label>
                        <div class="col-sm-2 required">
                            <select class="form-control" name="keamanan" id="keamanan">
                                <option value="biasa" {{ ($draft->keamanan == 'biasa') ? 'selected' : '' }}>Biasa</option>
                                <option value="rahasia" {{ ($draft->keamanan == 'rahasia') ? 'selected' : '' }}>Rahasia</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Tujuan</label>
                        <div class="col-sm-8">
                            <textarea id="tujuan" name="tujuan" class="form-control" style="height: 50px" placeholder="Pihak tujuan surat">{{ $draft->tujuan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Unit Konseptor</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="unit_konseptor" id="unit_konseptor">
                                @foreach($unit as $u)
                                <option value="{{ $u->id}}" {{ ($u->id == $draft->unit_id) ? 'selected': '' }}>{{ $u->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Penandatangan</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="pejabat" id="pejabat">
                                @foreach($pejabat as $p)
                                <option value="{{ $p->id}}" {{ ($u->id == $draft->pejabat_id) ? 'selected': '' }}>{{ $p->nama }} - {{ $p->unit->jbtn or '' }} ({{ $p->jbtn_status}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <textarea id="keterangan" name="keterangan" class="form-control" style="height: 50px" placeholder="Alasan/pertimbangan pembuatan surat">{{ $draft->notes }}</textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a id="link-batal" href="{{ route('show-draft',[encrypt($draft->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Simpan</button>
                    </div>

                </div>
            </form>
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->


    </div>

</div>
<script>
    $(document).ready(function () {
        $("#sifat").select2();
        $("#keamanan").select2();
        $("#unit_konseptor").select2();
        $("#pejabat").select2();
        $("#nomor").focus();
        
        $('#link-batal').click(function(e) {
          e.preventDefault();
          var $link = $(this);
          bootbox.confirm("Yakin akan membatalkan edit data ?", function (confirmation) {
              confirmation && document.location.assign($link.attr('href'));
          });        
        });
        
        $('#form-simpan-draft').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin simpan perubahan draft / net ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
    });

</script>

@endsection