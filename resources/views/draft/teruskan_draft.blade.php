@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Penerusan Draft / Net</h3>
            </div>
            <!-- /.box-header -->
            <form id="form-teruskan" class="form-horizontal" action="{{route('doTeruskan-draft')}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $draft->id }}">
                <div class="box-body">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Penerima</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="receiver" id="receiver">
                                @foreach($user as $p)
                                <option value="{{ $p->nip}}">{{ $p->nama }} (NIP.{{ $p->nip }}) </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan penerusan" maxlength="255">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> Data harus diisi seluruhnya.</span> 
                    <div class="pull-right">
                        <a href="{{ route('show-draft',[encrypt($draft->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Simpan</button>
                    </div>

                </div>
            </form>
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->


    </div>

</div>
<script>
    $(document).ready(function () {
        $("#receiver").select2();
        
        $('#form-teruskan').submit(function(e) {
            var currentForm = this;
            var nama = $("#receiver :selected").text();
            e.preventDefault();
            bootbox.confirm("Yakin teruskan draft/net ke <strong>"+nama+"</strong> ?", function(result) {
                if (result) {
                     bootbox.confirm("Sekali lagi, Yakin teruskan draft/net ke <strong>"+nama+"</strong> ?", function(result) {
                        if (result) {
                            currentForm.submit();
                        }
                    });
                }
            });
        });
    });

</script>

@endsection