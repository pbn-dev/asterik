@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Rekam Draft / Net Surat Baru</h3>
            </div>
            <!-- /.box-header -->
                        
            <form id="form-simpan-draft" class="form-horizontal" action="{{route('store-draft')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Nomor</label>
                        <div class="col-sm-4">
                            <input maxlength="255" class="form-control" name="nomor" id="nomor" placeholder="contoh: S-    /WPB.01/KP.01/2017" value="{{ old('nomor') }}">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Hal</label>
                        <div class="col-sm-8">
                            <textarea id="hal" name="hal" class="form-control" style="height: 50px" placeholder="perihal surat">{{ old('hal') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Sifat</label>
                        <div class="col-sm-2 required">
                            <select class="form-control" name="sifat" id="sifat">
                                <option value="biasa" {{ (old('sifat') == "biasa") ? 'selected' : '' }}>Biasa</option>
                                <option value="segera" {{ (old('sifat') == "segera") ? 'selected' : '' }}>Segera</option>
                                <option value="sangat segera" {{ (old('sifat') == "sangat segera") ? 'selected' : '' }}>Sangat Segera</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keamanan</label>
                        <div class="col-sm-2 required">
                            <select class="form-control" name="keamanan" id="keamanan">{
                                <option value="biasa" {{ (old('keamanan') == "biasa") ? 'selected' : '' }}>Biasa</option>
                                <option value="rahasia" {{ (old('keamanan') == "rahasia") ? 'selected' : '' }}>Rahasia</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Tujuan</label>
                        <div class="col-sm-8">
                            <textarea id="tujuan" name="tujuan" class="form-control" style="height: 50px" placeholder="Pihak tujuan surat">{{ old('tujuan') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Unit Konseptor</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="unit_konseptor" id="unit_konseptor">
                                @foreach($unit as $u)
                                <option value="{{ $u->id}}" {{ ($u->id == Auth::user()->unit_id) ? 'selected': '' }}>{{ $u->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Penandatangan</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="pejabat" id="pejabat">
                                @foreach($pejabat as $p)
                                <option value="{{ $p->id}}">{{ $p->nama }} - {{ $p->unit->jbtn or '' }} ({{ $p->jbtn_status}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <textarea id="keterangan" name="keterangan" class="form-control" style="height: 50px" placeholder="Alasan/pertimbangan pembuatan surat">{{ old('keterangan') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Lokasi File Draft:</label>
                        <div class="col-sm-6">
                            <input type="file" id="file" name="file">
                        </div>   
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <span class="pull-left">
                        <span class="red">*</span> 
                        Data harus diisi seluruhnya. File harus dipilih dalam format dokumen (docx, doc, xlsx, xls, ppt, pptx) maksimal 10 MB.</span> 
                        
                    <div class="pull-right">
                        <a href="{{ route('draft') }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Simpan</button>
                    </div>

                </div>
                
                <div class="box-body">
                <div class="callout callout-info">
                <h4>Info!</h4>
                <ul>
                    <li>Draft / Net yang akan Anda buat, hanya dapat disetujui oleh penandatangan, dan dapat dibataalkan selama belum direkam data pengiriman surat.</li>
                    <li>Agar Draft / Net yang telah dibuat dapat disetujui, maka harus di-upload file Net terlebih dahulu atau dengan memilih dari file Draft yang tersedia. Untuk saat ini silahkan upload file Draft terlebih dahulu.</li>
                    <li>Draft / Net yang telah disetujui, maka kewajiban Anda untuk mencatat tanggal / nomor surat, tanggal pengiriman, serta data pengarsipan.</li>
                    <li>Untuk Draft / Net yang keamanannya bersifat rahasia, maka hanya dapat dilihat oleh Anda, penandatangan, dan user yang mendapat penerusan. Sementara untuk Draft / Net yang keamanannya biasa, dapat dilihat pula oleh user yang unitnya sama atau lebih tinggi. </li>
                </ul>
              </div>
            </div>
            </form>
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->


    </div>

</div>
<script>
    $(document).ready(function () {
        $("#sifat").select2();
        $("#keamanan").select2();
        $("#unit_konseptor").select2();
        $("#pejabat").select2();
        $("#nomor").focus();
        
        $('#form-simpan-draft').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin simpan draft / net ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
    });

</script>

@endsection