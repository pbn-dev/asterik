@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">

        	<div class="box-header with-border">

        		<h3 class="box-title">Upload Draft / Net
                    <small></small>
                </h3>

        	</div> <!-- /.box-header -->

            <form id="form-upload-draft" class="form-horizontal" action="{{ route('doUpload-draft') }}" method="POST" enctype="multipart/form-data">

                <div class="box-body">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="id" value="{{ $draft->id }}">
                                        
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Lokasi file softcopy:</label>
                        <div class="col-sm-6">
                            <input type="file" id="file" name="file">
                        </div>   
                    </div> 
                    
                     <div class="form-group required">
                        <label class="col-sm-2 control-label">Jenis File</label>
                        <div class="col-sm-2 required">
                            <select class="form-control" name="jenis" id="jenis">
                                <option value="">-</option>
                                <option value="draft" {{ (old('jenis') == "draft") ? 'selected' : '' }}>Draft</option>
                                <option value="net" {{ (old('jenis') == "net") ? 'selected' : '' }}>Net</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <input type="text" id="keterangan" name="keterangan" class="form-control" maxlength="255" value="{{ old('keterangan')}}">
                        </div>
                    </div>
                    
                </div> <!-- /.box-body -->

                <div class="box-footer">
                    <span class="pull-left"><span class="red">*</span> File harus dipilih dalam format dokumen (docx, doc, xlsx, xls, pptx, ppt) maksimal 10 MB</span> 
                    <div class="pull-right">
                       <a href="{{ route('show-draft',[encrypt($draft->id)]) }}" class="btn btn-danger"><i class="fa fa-times"></i> Batal</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div> <!-- /.box-footer -->

            </form>
            
        </div> <!-- /.box -->      

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        
        $("#jenis").select2();
        $('#form-upload-draft').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin upload file draft / net ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });
        
    });
</script>
@endsection