@extends('layout.master')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Draft / Net Surat</h3>

                <div class="box-tools pull-right">
                    <a class="btn btn-xs btn-info" href="{{ route('create-draft') }}" data-toggle="tooltip" title="Create new draft"><i class="fa fa-plus"></i> Buat Baru</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive mailbox-messages">
                    <table id="tabel" class="table table-hover table-striped table-borderless" width="100%" cellspacing="0">
                        <thead
                            <tr>
                                <th width="3%">#</th>                               
                                <th width="30%">Nomor / Hal / Sifat</th>                                
                                <th width="15%">Tujuan</th>
                                <th width="15%">Unit Konseptor</th>
                                <th width="15%">Penandatangan</th>
                                <th width="22%">Keterangan</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($draft as $dr)
                            <tr>
                                <td>{{ $i }}</td>

                                <td class="mailbox-subject">
                                    <?php
                                    $cls = 'black';
                                    if ($dr->latestMailSent == "") {
                                        if ($dr->created_by == Auth::user()->nip) {
                                            $cls = "";
                                        }
                                    } else {
                                        if ($dr->latestMailSent->receiver == Auth::user()->nip) {
                                            $cls = "";
                                        }
                                    }
                                    ?>

                                    <a class="{{ $cls }}" href="{{ route('show-draft', [encrypt($dr->id)]) }}"><b>{{ $dr->nomor }}</b></a> -
                                    {{ $dr->hal }}

                                    <br /><br />

                                    @if($dr->sifat == 'biasa')
                                    <span class='label label-default'><i><small>{{ ucwords($dr->sifat) }}</small></i></span>
                                    @else
                                    <span class='label bg-orange'><i><small>{{ ucwords($dr->sifat) }}</small></i></span>
                                    @endif

                                    @if($dr->keamanan == 'rahasia')
                                    <span class='label bg-orange' data-toggle='tooltip' title='rahasia'><i class='fa fa-lock'></i></span>
                                    @endif

                                    <span class='label label-default'><i><small>Ref.{{ $dr->ref }}</small></i></span>

                                    @if($dr->tanggal != null)
                                    <i><span class="label label-success">Tgl. {{ date('d-m-Y', strtotime($dr->tanggal)) }}</span></i>
                                    @else
                                    @if($dr->approved_at != null)
                                    <i><span class="label label-danger">Tgl. belum diisi <i class="fa fa-exclamation"></i></span></i>
                                    @endif
                                    @endif
                                </td>
                                <td class="mailbox-name">
                                    {{ $dr->tujuan }}
                                    <br /><br />
                                    @if($dr->sent_at != null)
                                    <i><span class="label label-success">{{ date('d-m-Y', strtotime($dr->tanggal)) }}</span></i>
                                    @else
                                    @if($dr->tanggal != null)
                                    <i><span class="label label-danger">Belum dikirim <i class="fa fa-exclamation"></i></span></i>
                                    @endif
                                    @endif
                                </td>
                                <td class="mailbox-name">
                                    {{ $dr->unit->nama or '' }} 
                                    <br /><br />
                                    <i><small> Dibuat oleh: {{ $dr->creator->nama or '' }} ({{ date('d-m-Y H:i:s', strtotime($dr->created_at)) }})</small></i>
                                </td>
                                <td>
                                    {{ $dr->pejabat->nama or '' }} - {{ $dr->pejabat->unit->jbtn or '' }} {{ ($dr->pejabat->jbtn_status == 'definitif') ?  '' : '('.$dr->pejabat->jbtn_status.')' }}
                                    <br /><br />
                                    @if($dr->approved_at == null)
                                    <i><span class="label label-danger">Belum disetujui <i class="fa fa-exclamation"></i></span></i>
                                    @else
                                    <i><span class="label label-success"> {{ date('d-m-Y H:i:s', strtotime($dr->approved_at))}}</span></i>
                                    @endif

                                </td>
                                <td class="mailbox-name">
                                    {{ $dr->notes }}
                                    <br /><br />
                                    Posisi Terakhir: <br />
                                    <i><small>
                                            @if($dr->latestMailSent != "")            
                                            {{ $dr->latestMailSent->userReceiver->nama }} ({{ date('d-m-Y H:i:s', strtotime($dr->latestMailSent->created_at)) }})
                                            @if($dr->latestMailSent->read_at == "")
                                            &checkmark;
                                            @else
                                            <span class="text-green"><strong>&checkmark;</strong></span>
                                            @endif
                                            @else
                                            {{ $dr->creator->nama or '' }} ({{ date('d-m-Y H:i:s', strtotime($dr->created_at)) }}) <span class="text-green"><strong>&checkmark;</strong></span>
                                            @endif
                                        </small></i>
                                </td>

                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table -->
                </div>
                <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /. box -->


    </div>

</div>
<script type="text/javascript">
    
//    $('#tabel').DataTable({
//      "paging": false,
//      "lengthChange": false,
//      "searching": true,
//      "ordering": true,
//      "info": false,
//      "autoWidth": true,
//      "scrollX": false
//    });
    
</script>

@endsection