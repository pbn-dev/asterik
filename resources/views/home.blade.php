<div class='row'>    

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $data['sum_mail'] }}</h3>

                <p>Draft / Net</p>
            </div>
            <div class="icon">
                <i class="fa fa-file-text-o"></i>
            </div>
            <a href="{{ route('draft') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ $data['sum_created'] }}</h3>

                <p>Saya Buat</p>
            </div>
            <div class="icon">
                <i class="fa fa-envelope-o"></i>
            </div>
            <a href="{{ route('draft-created-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{{ $data['sum_received'] }}</h3>

                <p>Belum Saya Baca</p>
            </div>
            <div class="icon">
                <i class="fa fa-eye-slash"></i>
            </div>
           <a href="{{ route('draft-not-read-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ $data['sum_not_forwarded'] }}</h3>

                <p>Belum Saya Teruskan</p>
            </div>
            <div class="icon">
                <i class="fa fa-mail-forward"></i>
            </div>
            <a href="{{ route('draft-not-forwarded-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>

</div>

<div class='row'>
    <div class="col-md-3">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>{{ $data['sum_not_signed'] }}</h3>

                <p>Belum Saya Setujui</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
            <a href="{{ route('draft-not-approved-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
        <div class="small-box bg-purple">
            <div class="inner">
                <h3>{{ $data['sum_not_sent'] }}</h3>

                <p>Belum Saya Kirim</p>
            </div>
            <div class="icon">
                <i class="fa fa-send"></i>
            </div>
            <a href="{{ route('draft-not-sent-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <!-- small box -->
        <div class="small-box bg-olive">
            <div class="inner">
                <h3>{{ $data['sum_not_date'] }}</h3>

                <p>Belum Saya Beri Tanggal</p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar"></i>
            </div>
            <a href="{{ route('draft-not-given-date-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
        <div class="small-box bg-maroon">
            <div class="inner">
                <h3>{{ $data['sum_not_archived'] }}</h3>

                <p>Belum Saya Arsipkan</p>
            </div>
            <div class="icon">
                <i class="fa fa-archive"></i>
            </div>
            <a href="{{ route('draft-not-archived-by-user') }}" class="small-box-footer">
                info lanjut <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Progress Penyelesaian Draft / Net</h3>
            </div>
            <div class="box-body">
                <br />
                <div class="progress-group">
                    <span class="progress-text">Belum disetujui</span>
                    <span class="progress-number"><b>{{ $data['sum_mail_not_approved'] }}</b>/{{ $data['sum_mail'] }}</span>

                    <div class="progress sm">
                        <div class="progress-bar progress-bar-aqua" style="width: {{ $data['sum_mail_not_approved_bar'] }}%"></div>
                    </div>
                </div>
                <div class="progress-group">
                    <span class="progress-text">Belum diberi tanggal</span>
                    <span class="progress-number"><b>{{ $data['sum_mail_not_date'] }}</b>/{{ $data['sum_mail'] }}</span>

                    <div class="progress sm">
                        <div class="progress-bar progress-bar-red" style="width: {{ $data['sum_mail_not_date_bar'] }}%"></div>
                    </div>
                </div>
                <div class="progress-group">
                    <span class="progress-text">Belum dikirim</span>
                    <span class="progress-number"><b>{{ $data['sum_mail_not_sent'] }}</b>/{{ $data['sum_mail'] }}</span>

                    <div class="progress sm">
                        <div class="progress-bar progress-bar-yellow" style="width: {{ $data['sum_mail_not_sent_bar'] }}%"></div>
                    </div>
                </div>
                <div class="progress-group">
                    <span class="progress-text">Belum diarsipkan</span>
                    <span class="progress-number"><b>{{ $data['sum_mail_not_archived'] }}</b>/{{ $data['sum_mail'] }}</span>

                    <div class="progress sm">
                        <div class="progress-bar progress-bar-green" style="width: {{ $data['sum_mail_not_archived_bar'] }}%"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
