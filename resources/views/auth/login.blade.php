<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} - Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}" >
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}'>
  <!-- Font Awesome -->
  <link rel="stylesheet" href='{{ asset("/bower_components/font-awesome/css/font-awesome.min.css")}}'>
  <!-- Ionicons -->
  <link rel="stylesheet" href='{{ asset("/bower_components/Ionicons/css/ionicons.min.css")}}'>
  <!-- Theme style -->
  <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}'>
  <!-- my-style -->
    <link rel="stylesheet" href='{{ asset("/css/login.css")}}' type="text/css" />

  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-header">
      -- LOGIN --
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><img src="{{ asset('/img/logo.png') }}" style="max-width:200px;max-height:80px"/></p>

    @if (session('message_warning'))
      <div class="alert alert-warning">
        <i class="fa fa-warning"></i> {{ session('message_warning') }}
      </div>
    @endif
      
    @if(count($errors) > 0)
      <div class="alert alert-warning">
        @foreach ($errors->all() as $error)
              <i class="fa fa-warning"></i> {{ $error }} <br/>
        @endforeach
      </div>
    @endif

    <form action="{{ route('doLogin') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
         
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <!--p>- OR -</p-->
      <!--a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a-->
      <!--a href="" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a-->
    </div>
    <!-- /.social-auth-links -->

    <!--a href="#">Lupa password ?</a><br-->
    <!--a href="register.html" class="text-center">Register a new membership</a-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src='{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jQuery-2.2.3.min.js") }}'></script>
<!-- Bootstrap 3.3.6 -->
<script src='{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}'></script>
<!-- AdminLTE App -->
<script src='{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}'></script>
<script>
  $(document).ready(function() {

        $('div.alert').delay(5000).slideUp(500);
    
    });
</script>
</body>
</html>
