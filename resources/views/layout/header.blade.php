<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('beranda') }}" class="logo"><img src="{{ asset('/img/logo.png') }}" style="max-height:40px; max-width:200px "/></a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        @if(Auth::check())
                        <img src="{{ session('foto') }}" class="user-image" alt="User Image"/>
                        @endif
                        <!--i class=" fa fa-user-circle fa-lg"></i-->
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">
                            @if(Auth::check())
                                {{-- strtok(Auth::user()->nama, " ") --}}&nbsp;
                            @endif
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ session('foto') }}" class="img-circle" alt="User Image" />
                            <p>
                                @if(Auth::check())
                                    {{ Auth::user()->nama }}
                                    <small>Member since {{ date('M. Y', strtotime(Auth::user()->created_at)) }}</small>
                                @endif
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat link-logout">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>