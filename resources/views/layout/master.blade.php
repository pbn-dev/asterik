<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <meta name="_token" content="{{ csrf_token() }}" charset="UTF-8">
    <title>{{ config('app.name') }} - {{ str_replace('-', ' ', title_case(Route::currentRouteName())) }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}">

    <!-- jquery-ui -->
    <link rel="stylesheet" href='{{ asset("/bower_components/jquery-ui/themes/base/jquery-ui.min.css") }}' type="text/css">
    <!-- Bootstrap 3.3.2 -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}' type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href='{{ asset("/bower_components/font-awesome/css/font-awesome.min.css")}}' type="text/css">
    <!-- Ionicons -->
    <link rel="stylesheet" href='{{ asset("/bower_components/Ionicons/css/ionicons.min.css")}}' type="text/css">
    <!-- Data Tables -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}' type="text/css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datepicker/datepicker3.css") }}' type="text/css">
    <!-- bootstrap select2 -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css") }}' type="text/css">
    <!-- Pace style -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/pace/pace.min.css")}}' type="text/css">
    <!-- Theme style -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}' type="text/css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/dist/css/skins/_all-skins.min.css")}}' type="text/css" />
    <!-- my-style -->
    <link rel="stylesheet" href='{{ asset("/css/style.css")}}' type="text/css" />
    

    <!-- HTML5  Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.2.3 -->
    <script src='{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}'></script>
    <script src='{{ asset ("/bower_components/jquery-ui/jquery-ui.min.js") }}'></script>
    <!-- Bootstrap 3.3.6 -->
    <script src='{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}'></script>
    <!-- Select2 -->
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/select2/select2.full.js") }}'></script>
    <!-- Data Table -->
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}'></script>
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}'></script>
    <!-- PACE -->
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/pace/pace.min.js") }}'></script>
    <!-- SlimScroll -->
    <!-- bootstrap date picker -->
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js") }}'></script>
    <script src='{{ asset ("/bower_components/AdminLTE/plugins/datepicker/locales/bootstrap-datepicker.id.js") }}'></script>
    
    <!-- bootbox -->
    <script src='{{ asset ("/js/bootbox.min.js") }}'></script>    
    <!-- AdminLTE App -->
    <script src='{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}'></script>
    


<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</head>
<body class="skin-blue">
<div class="wrapper" id="app">

    <!-- Header -->
    @include('layout.header')

    <!-- Sidebar -->
    @include('layout.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!--section class="content-header"-->
            <!-- You can dynamically generate breadcrumbs here -->
            <!--ol class="breadcrumb">
                <li><a href="#" onClick="window.location.href='{{ URL::previous() }}'">
                    Kembali
                </a></li>
                <li class="active">Here</li>
            </ol-->
        <!--/section-->
        <section class="content-header">
            @yield('content-header')
        </section>

        <!-- Main content -->
        <section class="content">
            @if(session('message_success'))
                <div class="alert alert-success"><i class="fa fa-check"></i><em> {{ session('message_success') }}</em></div>
              @endif
              @if(Session::has('message_warning'))
                <div class="alert alert-warning"><i class="fa fa-warning"></i><em> {{ session('message_warning') }}</em></div>
              @endif
              
            @if(session('errors'))
             @if(count($errors) > 0)
              <div class="alert alert-warning">
                <ul>
                @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
             @endif
            @endif
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('layout.footer')

</div><!-- ./wrapper -->

</body>
</html>

<script>

    $(document).ready(function() {

        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}'} });

        $('div.alert').delay(8000).slideUp(500);

        $.fn.datepicker.defaults.language = 'id';
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
            //startDate:'01-01-2007'
          });

        $('#form-simpan').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            bootbox.confirm("Yakin simpan data ?", function(result) {
                if (result) {
                    currentForm.submit();
                }
            });
        });

        $('.link-delete').click(function(e) {
          e.preventDefault();
          var $link = $(this);
          bootbox.confirm("Yakin akan hapus data ?", function (confirmation) {
              confirmation && document.location.assign($link.attr('href'));
          });        
        });

        $('.link-logout').click(function(e) {
          e.preventDefault();
          var $link = $(this);
          bootbox.confirm("Yakin akan Logout ?", function (confirmation) {
              confirmation && document.location.assign($link.attr('href'));
          });        
        });
    
    });
</script>