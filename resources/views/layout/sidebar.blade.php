<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <!--div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                < Status >
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div-->

        <!-- search form (Optional) -->
        <!--form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <?php $url = explode("/",Request::url()); ?>
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            @if(Auth::check())
            <li class="{{ (in_array('beranda', $url) == true) ? 'active' : ''  }}"><a href="{{ route('beranda') }}"><i class="fa fa-home"></i><span>Beranda</span></a></li>
            <li class="{{ (in_array('draft', $url) == true) ? 'active' : ''  }}"><a href="{{ route('draft') }}"><i class="fa fa-file-text-o"></i>Draft / Net</a></li>
            <li class="{{ (in_array('arsip', $url) == true) ? 'active' : ''  }}"><a href="{{ route('arsip') }}"><i class="fa fa-archive"></i>Arsip</a></li>

            @if(Auth::user()->role_id == '10')
            <li class="treeview {{ (in_array('admin', $url) == true) ? 'active' : ''  }}">
                <a href="javascript:void(0)"><i class="fa fa-cogs"></i>Admin <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (in_array('unit', $url) == true) ? 'active' : ''  }}"><a href="{{ route('unit') }}">&nbsp;&nbsp;&nbsp;<i class="fa fa-bank"></i>Unit</a></li>
                    <li class="{{ (in_array('user', $url) == true) ? 'active' : ''  }}"><a href="{{ route('user') }}">&nbsp;&nbsp;&nbsp;<i class="fa fa-users"></i>User</a></li>
                    <li class="{{ (in_array('pejabat', $url) == true) ? 'active' : ''  }}"><a href="{{ route('pejabat') }}">&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>Pejabat</a></li>
                    <li class="{{ (in_array('log', $url) == true) ? 'active' : ''  }}"><a href="{{ route('login-log') }}">&nbsp;&nbsp;&nbsp;<i class="fa fa-history"></i>Log</a></li>
                    <li class="{{ (in_array('logo', $url) == true) ? 'active' : ''  }}"><a href="{{ route('logo') }}">&nbsp;&nbsp;&nbsp;<i class="fa fa-image"></i>Logo dan Favicon</a></li>
                </ul>
            </li>
            @endif
            @endif
            
        </ul><!-- /.sidebar-menu -->
    </section>
    {{ count($url) }}
    <!-- /.sidebar -->
</aside>