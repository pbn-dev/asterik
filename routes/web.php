<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'HomeController@index')->middleware('auth');
Route::get('/beranda', 'HomeController@beranda')->name('beranda')->middleware('auth');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard')->middleware('auth');


Route::get('login', 'LoginController@showLoginForm')->name('login');
Route::post('doLogin', 'LoginController@login')->name('doLogin');
Route::get('logout', 'LoginController@logout')->name('logout')->middleware('auth');

Route::get('/profile', 'HomeController@profile')->name('profile')->middleware('auth');
Route::post('/profile', 'HomeController@doProfile')->name('doProfile')->middleware('auth');

Route::get('changePassword', 'UserController@changePassword')->name('change-password')->middleware('auth');
Route::post('doChangePassword', 'UserController@doChangePassword')->name('doChange-password')->middleware('auth');

Route::group(['prefix' => 'draft'], function() {
    
    Route::get('/', 'DraftController@index')->name('draft')->middleware('auth');
    Route::get('/draft-list', 'DraftController@draft')->name('draft-list')->middleware('auth');
    Route::get('/create', 'DraftController@create')->name('create-draft')->middleware('auth');
    Route::post('/store', 'DraftController@store')->name('store-draft')->middleware('auth');
    Route::get('/edit/{id}', 'DraftController@edit')->name('edit-draft')->middleware('auth');
    Route::post('/update', 'DraftController@update')->name('update-draft')->middleware('auth');
    Route::get('/show/{id}', 'DraftController@show')->name('show-draft')->middleware('auth');
    Route::get('/remove/{id}', 'DraftController@destroy')->name('remove-draft')->middleware('auth');
    
    Route::get('/approved/{id}', 'DraftController@approved')->name('approved-draft')->middleware('auth');
    Route::get('/disApproved/{id}', 'DraftController@disApproved')->name('disApproved-draft')->middleware('auth');
    Route::get('/rekam-tanggal/{id}', 'DraftController@rekamTanggal')->name('rekam-tanggal')->middleware('auth');
    Route::post('/doRekam-tanggal', 'DraftController@doRekamTanggal')->name('doRekam-tanggal')->middleware('auth');
    Route::get('/kirim/{id}', 'DraftController@kirim')->name('kirim-mail')->middleware('auth');
    Route::post('/doKirim', 'DraftController@doKirim')->name('doKirim-mail')->middleware('auth');
    Route::get('/arsip/{id}', 'DraftController@arsip')->name('arsip-mail')->middleware('auth');
    Route::post('/doArsip', 'DraftController@doArsip')->name('doArsip-mail')->middleware('auth');
    
    Route::group(['prefix' => 'upload'], function(){
        Route::get('/draft/{id}', 'FileController@uploadDraft')->name('upload-draft')->middleware('auth');
        Route::post('/doDraft', 'FileController@doUploadDraft')->name('doUpload-draft')->middleware('auth');
        Route::get('/pendukung/{id}', 'FileController@uploadPendukung')->name('upload-pendukung')->middleware('auth');
        Route::post('/doPendukung', 'FileController@doUploadPendukung')->name('doUpload-pendukung')->middleware('auth');
        
    });
    
    Route::get('/teruskan-draft/{id}', 'MailSentController@teruskan')->name('teruskan-draft')->middleware('auth');
    Route::post('/doTeruskan-draft', 'MailSentController@doTeruskan')->name('doTeruskan-draft')->middleware('auth');
    Route::get('/remove-mailsent/{id}', 'MailSentController@destroyMailSent')->name('remove-mailsent')->middleware('auth');
    
    Route::get('/draft-create-by-user', 'DraftController@draftCreatedByUser')->name('draft-created-by-user')->middleware('auth');
    Route::get('/draft-not-read-by-user', 'DraftController@draftNotReadByUser')->name('draft-not-read-by-user')->middleware('auth');
    Route::get('/draft-not-forwarded-by-user', 'DraftController@draftNotForwardedByUser')->name('draft-not-forwarded-by-user')->middleware('auth');
    Route::get('/draft-not-approved-by-user', 'DraftController@draftNotApprovedByUser')->name('draft-not-approved-by-user')->middleware('auth');
    Route::get('/draft-not-given-date-by-user', 'DraftController@draftNotGivenDatedByUser')->name('draft-not-given-date-by-user')->middleware('auth');
    Route::get('/draft-not-sent-by-user', 'DraftController@draftNotSentByUser')->name('draft-not-sent-by-user')->middleware('auth');
    Route::get('/draft-not-archived-by-user', 'DraftController@draftNotArchivedByUser')->name('draft-not-archived-by-user')->middleware('auth');
});

Route::group(['prefix' => 'arsip'], function() {
    Route::get('/', 'ArsipController@index')->name('arsip')->middleware('auth');
    Route::get('/{id}', 'ArsipController@arsip')->name('arsip-tahun')->middleware('auth');
    Route::get('/show/{id}', 'ArsipController@show')->name('show-arsip')->middleware('auth');
    Route::get('/pindah/{id}', 'ArsipController@pindah')->name('pindah-arsip')->middleware('auth');
    Route::post('/pindah', 'ArsipController@doPindah')->name('doPindah-arsip')->middleware('auth');
    Route::get('/upload-arsip/{id}', 'FileController@uploadArsip')->name('upload-arsip')->middleware('auth');
    Route::post('/doUpload-arsip', 'FileController@doUploadArsip')->name('doUpload-arsip')->middleware('auth');
    
    
});

//admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth','isAdmin']], function() {

    Route::group(['prefix' => 'unit'], function() {
        Route::get('/', 'UnitController@index')->name('unit');
        Route::get('/change-status/{id}', 'UnitController@changeStatus')->name('change-status-unit');
        Route::get('rekam', 'UnitController@unitRekam')->name('rekam-unit');
        Route::post('doRekam', 'UnitController@doRekamUnit')->name('doRekam-unit');
        Route::get('edit/{nip}', 'UnitController@unitEdit')->name('edit-unit');
        Route::post('doEdit', 'UnitController@doEditUnit')->name('doEdit-unit');
    });
    Route::group(['prefix' => 'user', 'middleware' => 'auth'], function() {
        Route::get('/', 'UserController@user')->name('user');
        Route::get('/change-status/{id}', 'UserController@changeStatus')->name('change-status-user');
        Route::get('rekam', 'UserController@userRekam')->name('rekam-user');
        Route::post('doRekam', 'UserController@doRekamUser')->name('doRekam-user');
        Route::get('edit/{nip}', 'UserController@userEdit')->name('edit-user');
        Route::post('doEdit', 'UserController@doEditUser')->name('doEdit-user');
        Route::get('delete/{nip}', 'UserController@doHapusUser')->name('delete-user');
        Route::get('resetPass/{nip}', 'UserController@doResetPassUser')->name('resetPass-user');
    });

    Route::group(['prefix' => 'pejabat', 'middleware' => 'auth'], function() {
        Route::get('/', 'PejabatController@index')->name('pejabat');
        Route::get('rekam', 'PejabatController@create')->name('rekam-pejabat');
        Route::post('doRekam', 'PejabatController@store')->name('doRekam-pejabat');
        Route::get('/change-status/{id}', 'PejabatController@changeStatus')->name('change-status-pejabat');
        Route::get('edit/{id}', 'PejabatController@edit')->name('edit-pejabat');
        Route::post('doEdit', 'PejabatController@update')->name('doEdit-pejabat');
        Route::get('delete/{id}', 'PejabatController@destroy')->name('delete-pejabat');

    });
    
    Route::group(['prefix' => 'logo'], function() {
        Route::get('/', 'LogoController@index')->name('logo');
        Route::post('/upload', 'LogoController@upload')->name('upload-logo');
        Route::post('/upload-favicon', 'LogoController@uploadFavicon')->name('upload-favicon');
    });

});

Route::group(['prefix' => 'file', 'middleware' => ['auth']], function(){
	Route::get('show/{id}', 'FileController@show')->name('show-file');
        Route::get('remove/{id}', 'FileController@remove')->name('remove-file');
        Route::get('change-tipe/{id}', 'FileController@changeTipe')->name('change-tipe-file');
});  
    
    //api
Route::group(['prefix' => 'apis'], function(){
	Route::get('user/get-nama-like-nip', 'UserController@getNamaLikeNip')->name('get-nama-user-like-nip');
});

Route::group(['prefix' => 'log'], function() {
    Route::get('/', 'LogController@index')->name('login-log')->middleware('auth');
});



