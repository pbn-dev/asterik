<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $table = 'user';
    
    public function role(){
        return $this->belongsTo('App\Role');
    }
    
     public function unit(){
        return $this->belongsTo('App\Unit');
    }
    
    public function loginLog(){
        return $this->hasMany('App\LoginLog','nip','nip');
    }
    
    public function mailCreator(){
        return $this->hasMany('App\Mail','created_by','nip');
    }
    
    public function mailSender(){
        return $this->hasMany('App\MailSent','sender','nip');
    }
    
    public function mailReceiver(){
        return $this->hasMany('App\MailSent','receiver','nip');
    }
    
    public function fileCreator(){
        return $this->hasMany('App\File','created_by','nip');
    }
    
    public function arsipCreator(){
        return $this->hasMany('App\MailArsip','created_by','nip');
    }
    
}
