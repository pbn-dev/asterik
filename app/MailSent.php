<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailSent extends Model {

    protected $table = 'mail_sent';
    public $timestamps = false;

    public function mail() {
        return $this->belongsTo('App\Mail');
    }
    
    public function userSender() {
        return $this->belongsTo('App\User','sender','nip');
    }
    
    public function userReceiver() {
        return $this->belongsTo('App\User','receiver','nip');
    }

}
