<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'file';
    
    public function mail() {
        return $this->belongsTo('App\Mail');
    }
    
    public function creator(){
        return $this->belongsTo('App\User','created_by','nip');
    }
}
