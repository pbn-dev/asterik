<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail;
use Auth;
use App\User;
use Validator;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('beranda');
    }
    
    public function beranda()
    {
        $pejabat = \App\Pejabat::where('nip', Auth::user()->nip)->where('active',1)->with('unit')->orderBy('jbtn_status','asc')->get();
        return view('index', ['pejabat' => $pejabat]);
    }
    
    public function dashboard()
    {
        // Sum Mail

        // get mail created by user 
        $draft1 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->where('created_by', Auth::user()->nip);
        // get mail sent to current user
        $draft2 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver', Auth::user()->nip);
                    $query->where('active','1');
                });
        // get mail where unit like user unit
        $draft3 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->where('unit_id', 'like', Auth::user()->unit_id . '%')
                ->where('keamanan', 'biasa');  // except secret mail
        // get mail signed by user who has plt. or Plh.
        $draft4 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip', Auth::user()->nip);
                })
                ->union($draft1)
                ->union($draft2)
                ->union($draft3)
                ->get();
        $sum_mail = count($draft4);

        // count mail created by user 
        $sum_created = Mail::where('active','1')
                ->whereNull('archived_at')
                ->where('created_by',Auth::user()->nip)
                ->count();
                //->orderBy('created_at','desc')
                //->get();
        
        // get mail signed by user
        $sum_signed = Mail::where('active','1')
            ->whereNull('archived_at')
            ->whereHas('pejabat', function($query) {
                $query->where('nip',Auth::user()->nip);
            })->count();
        
        // count mail received by current user but not read yet
        $sum_received = Mail::where('active','1')
                ->whereNull('archived_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver',Auth::user()->nip);
                    $query->whereNull('read_at');
                    $query->where('active','1');
                })->count();
                
        // get mail signed but not appproved by user
        $sum_not_signed = Mail::where('active','1')
            ->whereNull('archived_at')
            ->whereNull('approved_at')
            ->whereHas('pejabat', function($query) {
                $query->where('nip',Auth::user()->nip);
            })->count();
        
        // get mail received by current user but not forwarded yet
        $sum_not_forwarded1 = Mail::where('active','1')
            ->whereNull('archived_at')
            ->with('latestMailSent')->get();
        
        $snf = 0;
        foreach($sum_not_forwarded1 as $sp){
            if(isset($sp->latestMailSent->receiver)){
                if($sp->latestMailSent->receiver == Auth::user()->nip){
                    $snf++;
                }
            }
        }
                
        // get mail created by current user but not forwarded yet
        $sum_not_forwarded2 = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->doesntHave('latestMailSent')
            ->count();
        
        $sum_not_forwarded = $snf + $sum_not_forwarded2;
        
        // get mail created and has been approved but not given date by current user 
        $sum_not_date = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('approved_at')
            ->whereNull('tanggal')
            ->count();
        
        // get mail created and has been given date but not sent yet by current user 
        $sum_not_sent = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('tanggal')
            ->whereNull('sent_at')
            ->count();
        
        // get mail created and has been sent by current user but not archieved yet
        $sum_not_archived = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('sent_at')
            ->count();
        
        
        
        
        // sum mail not approved
        // get mail created by user 
        $draft1 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('approved_at')
                ->where('created_by', Auth::user()->nip);
        // get mail sent to current user
        $draft2 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('approved_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver', Auth::user()->nip);
                });
        // get mail where unit like user unit
        $draft3 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('approved_at')
                ->where('unit_id', 'like', Auth::user()->unit_id . '%')
                ->where('keamanan', 'biasa');  // except secret mail
        // get mail signed by user who has plt. or Plh.
        $draft4 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('approved_at')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip', Auth::user()->nip);
                })
                ->union($draft1)
                ->union($draft2)
                ->union($draft3)
                ->get();
        $sum_mail_not_approved = count($draft4);
        
        // sum mail not given date yet but have been approved
        // get mail created by user 
        $draft1 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('approved_at')
                ->whereNull('tanggal')
                ->where('created_by', Auth::user()->nip);
        // get mail sent to current user
        $draft2 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('tanggal')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver', Auth::user()->nip);
                });
        // get mail where unit like user unit
        $draft3 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('approved_at')
                ->whereNull('tanggal')
                
                ->where('unit_id', 'like', Auth::user()->unit_id . '%')
                ->where('keamanan', 'biasa');  // except secret mail
        // get mail signed by user who has plt. or Plh.
        $draft4 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('approved_at')
                ->whereNull('tanggal')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip', Auth::user()->nip);
                })
                ->union($draft1)
                ->union($draft2)
                ->union($draft3)
                ->get();
        $sum_mail_not_date = count($draft4);
        
        // sum mail not sent yet but have been given data
        // get mail created by user 
        $draft1 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('sent_at')
                ->whereNotNull('tanggal')
                ->where('created_by', Auth::user()->nip);
        // get mail sent to current user
        $draft2 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('sent_at')
                ->whereNotNull('tanggal')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver', Auth::user()->nip);
                });
        // get mail where unit like user unit
        $draft3 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('sent_at')
                ->whereNotNull('tanggal')
                ->where('unit_id', 'like', Auth::user()->unit_id . '%')
                ->where('keamanan', 'biasa');  // except secret mail
        // get mail signed by user who has plt. or Plh.
        $draft4 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNull('sent_at')
                ->whereNotNull('tanggal')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip', Auth::user()->nip);
                })
                ->union($draft1)
                ->union($draft2)
                ->union($draft3)
                ->get();
        $sum_mail_not_sent = count($draft4);
        
        // sum mail not archived yet but have been sent
        // get mail created by user 
        $draft1 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('sent_at')
                ->where('created_by', Auth::user()->nip);
        // get mail sent to current user
        $draft2 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('sent_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver', Auth::user()->nip);
                });
        // get mail where unit like user unit
        $draft3 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('sent_at')
                ->where('unit_id', 'like', Auth::user()->unit_id . '%')
                ->where('keamanan', 'biasa');  // except secret mail
        // get mail signed by user who has plt. or Plh.
        $draft4 = Mail::where('active', '1')
                ->whereNull('archived_at')
                ->whereNotNull('sent_at')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip', Auth::user()->nip);
                })
                ->union($draft1)
                ->union($draft2)
                ->union($draft3)
                ->get();
        $sum_mail_not_archived = count($draft4);
        
        $sum_mail_not_approved_bar = '0';
        $sum_mail_not_date_bar = '0';
        $sum_mail_not_sent_bar = '0';
        $sum_mail_not_archived_bar = '0';
        
        if($sum_mail > 0){
            $sum_mail_not_approved_bar = $sum_mail_not_approved * 100 / $sum_mail;
            $sum_mail_not_date_bar = $sum_mail_not_date * 100 / $sum_mail;
            $sum_mail_not_sent_bar = $sum_mail_not_sent * 100 / $sum_mail;
            $sum_mail_not_archived_bar = $sum_mail_not_archived * 100 / $sum_mail;
        }
        
        
        
        
        $data = [
            'sum_created' => $sum_created,
            'sum_received' => $sum_received,
            'sum_signed' => $sum_signed,
            'sum_not_signed' => $sum_not_signed,
            'sum_mail' => $sum_mail,
            'sum_not_forwarded' => $sum_not_forwarded,
            'sum_not_archived' => $sum_not_archived,
            'sum_mail_not_approved' => $sum_mail_not_approved,
            'sum_mail_not_date' => $sum_mail_not_date,
            'sum_mail_not_sent' => $sum_mail_not_sent,
            'sum_mail_not_archived' => $sum_mail_not_archived,
            'sum_mail_not_approved_bar' => $sum_mail_not_approved_bar,
            'sum_mail_not_date_bar' => $sum_mail_not_date_bar,
            'sum_mail_not_sent_bar' => $sum_mail_not_sent_bar,
            'sum_mail_not_archived_bar' => $sum_mail_not_archived_bar,
            'sum_not_sent' => $sum_not_sent,
            'sum_not_date' => $sum_not_date
        ];
                        
        return view('home', ['data' => $data]);
    }

    public function profile()
    {

        $nip = Auth::user()->nip;

        $user = User::where('nip',$nip)->with('unit')->with('role')->first();
        
        
        return view('user.edit_profile', ['user' => $user]);
    }

    public function doProfile(Request $request)
    {
        $rules = array(
                    'nip'    => 'required',
                    'nama'    => 'required',
                    'email' => 'required|email'
                );  

        //change atribute name
        $attributeNames = array(
            'nip'    => 'NIP',
            'nama'    => 'Nama',
            'email' => 'Email'
        );

        $messages = [
                        'required' => ':attribute harus diisi.',
                        'email'=> 'Email tidak valid.' 

                    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);

       
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }else{ 

            $user = User::where('nip',$_POST['nip'])->first();
            //$user->nip = $request->nip;
            $user->nama = $request->nama;
            $user->email = $request->email;
                                    
            $user->save();

            return redirect()->route('profile')
                ->with('message_success','Profile berhasil disimpan.');
            
        }
    }
}
