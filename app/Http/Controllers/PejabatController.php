<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Unit;

use App\Pejabat;

use Validator;

use Auth;

class PejabatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pejabat = Pejabat::orderBy('unit_id','asc')->with('unit')->where('active','1')->get();
        return view('pejabat.index',['pejabat' => $pejabat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = Unit::where('active', '1')->get();
        return view('pejabat.rekam',['unit' => $unit]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nip' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'status_jabatan' => 'required'
        ];
                
        $attributeNames = [
            'nip' => 'NIP',
            'nama' => 'Nama',
            'jabatan' => 'Jabatan',
            'status_jabatan' => 'Status Jabatan'
        ];
        
        $messages = [
            'require' => ':attribute harus diisi.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $pejabat = new Pejabat();
            $pejabat->nip = $request->nip;
            $pejabat->nama = $request->nama;
            $pejabat->unit_id = $request->jabatan;
            $pejabat->jbtn_status = $request->status_jabatan;
            if(Auth::check()){
                $pejabat->created_by = Auth::user()->nip;
            }
            $pejabat->active = '1';

            $pejabat->save();
            return redirect()->route('pejabat')
                            ->with('message_success', 'Pejabat ' . $request->nama . ' (NIP.' . $request->nip . ') berhasil disimpan.');
        }
        
        
        
    }
    
    public function changeStatus($id) {
        $idx = decrypt($id);

        $pejabat = Pejabat::where('id', $idx)->first();

        if ($pejabat->active == '1') {
            $pejabat->active = '0';
        } else {
            $pejabat->active = '1';
        }

 
        $pejabat->save();

        return redirect()->route('pejabat')
                        ->with('message_success', 'Ubah status pejabat berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idx = decrypt($id);

        $pejabat = Pejabat::where('id', $idx)->first();

        $unit = Unit::where('active', '1')->get();

        return view('pejabat.edit', ['unit' => $unit, 'pejabat' => $pejabat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $rules = [
             'id' => 'required',
            'nip' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'status_jabatan' => 'required'
        ];
                
        $attributeNames = [
            'nip' => 'NIP',
            'nama' => 'Nama',
            'jabatan' => 'Jabatan',
            'status_jabatan' => 'Status Jabatan'
        ];
        
        $messages = [
            'require' => ':attribute harus diisi.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $pejabat = Pejabat::where('id',$request->id)->first();
            $pejabat->nip = $request->nip;
            $pejabat->nama = $request->nama;
            $pejabat->unit_id = $request->jabatan;
            $pejabat->jbtn_status = $request->status_jabatan;
            
            $pejabat->save();
            return redirect()->route('pejabat')
                            ->with('message_success', 'Perubahan Pejabat ' . $request->nama . ' (NIP.' . $request->nip . ') berhasil disimpan.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idx = decrypt($id);

        $pejabat = Pejabat::where('id', '=', $idx)->first();
        Pejabat::where('id', '=', $idx)->delete();
        return redirect()->route('pejabat')
                    ->with('message_success', 'Pejabat ' . $pejabat->nama . ' berhasil dihapus.');
    }
}
