<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\Unit;

class UserController extends Controller {

    public function user() {
        $user = User::with('role')->with(['unit' => function ($query) {
                        $query->orderBy('id', 'asc');
                    }])->orderBy('active', 'desc')
                            ->orderBy('unit_id', 'asc')
                             ->orderBy('role_id', 'desc')
                            ->orderBy('nama', 'asc')->get();
        return view('user.index', ['user' => $user]);
    }

    public function userRekam() {
        $role = Role::where('active', '1')->get();
        $unit = Unit::where('active', '>', '0')->get();
        return view('user.rekam', ['role' => $role, 'unit' => $unit]);
    }

    public function doRekamUser(Request $request) {

        $rules = array(
            'nip' => 'required|digits:18|unique:user,nip',
            'nama' => 'required',
            'username' => 'required|unique:user,username',
            'password' => 'required|min:5|confirmed',
            'password_confirmation' => 'required|min:5',
            'role' => 'required',
            'unit' => 'required',
            'email' => 'required|unique:user,email|email'
        );

        //change atribute name
        $attributeNames = array(
            'nip' => 'NIP',
            'nama' => 'Nama',
            'nama' => 'Username',
            'password' => 'Password',
            'password_confirmation' => 'Konfirmasi Password',
            'role' => 'Role',
            'unit' => 'Unit',
            'email' => 'Email'
        );

        $messages = [
            'required' => ':attribute harus diisi.',
            'digits' => ':attribute harus :digits angka.',
            'confirmed' => 'Password tidak sama.',
            'password.min' => ':attribute minimal 5 karakter.',
            'password_confirmation.min' => ':attribute minimal 5 karakter.',
            'unique' => ':attribute sudah digunakan.',
            'email' => 'Email tidak valid.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);


        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $user = new User();
            $user->password = sha1(md5($request->password));
            $user->nip = $request->nip;
            $user->nama = $request->nama;
            $user->username = $request->username;
            $user->role_id = $request->role;
            $user->unit_id = $request->unit;
            $user->email = $request->email;
            $user->created_by = Auth::user()->nip;
            $user->active = '1';

            $user->save();
            return redirect()->route('user')
                            ->with('message_success', 'User ' . $request->nama . ' (NIP.' . $request->nip . ') berhasil disimpan.');
        }
    }

    public function changeStatus($nip) {
        $nipx = decrypt($nip);

        $user = User::where('nip', $nipx)->first();

        if ($user->active == '1') {
            $user->active = '0';
        } else {
            $user->active = '1';
        }

        if (Auth::check()) {
            $user->updated_by = Auth::user()->nip;
        }

        $user->save();

        return redirect()->route('user')
                        ->with('message_success', 'Ubah status user berhasil');
    }

    public function userEdit($nip) {

        $nipx = decrypt($nip);

        $user = User::where('nip', $nipx)->first();


        $role = Role::where('active', '1')->get();
        $unit = Unit::where('active', '>', '0')->get();
        
        

        return view('user.edit', ['role' => $role, 'unit' => $unit, 'user' => $user]);
    }

    public function doEditUser(Request $request) {

        $rules = array(
            'nip' => 'required',
            'nama' => 'required',
            'role' => 'required',
            'unit' => 'required',
            'email' => 'required|email'
        );

        //change atribute name
        $attributeNames = array(
            'nip' => 'NIP',
            'nama' => 'Nama',
            'role' => 'Role',
            'unit' => 'Unit',
            'email' => 'Email'
        );

        $messages = [
            'required' => ':attribute harus diisi.',
            'email' => 'Email tidak valid.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);



        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $email = User::where('email', $request->email)->where('nip', '!=', $request->nip)->count();

            if ($email > 0) {
                return back()
                                ->with('message_warning', 'Email sudah digunakan oleh user lain')
                                ->withInput();
            }

            $user = User::where('nip', $_POST['nip'])->first();

            if ($user == "") {
                return redirect()->route('error-404');
            }
            $user->nip = $request->nip;
            $user->nama = $request->nama;
            $user->role_id = $request->role;
            $user->unit_id = $request->unit;
            $user->email = $request->email;
            if (Auth::check()) {
                $user->updated_by = Auth::user()->nip;
            }

            $user->save();

            return redirect()->route('user')
                            ->with('message_success', 'Perubahan User ' . $request->nama . ' (NIP.' . $request->nip . ') berhasil disimpan.');
        }
    }

    public function doHapusUser($nip) {

        $nipx = decrypt($nip);

        $user = User::where('nip', '=', $nipx)->first();
        if ($user->role_id == '10') { // admin cannot be delete
            return redirect()->route('user')
                            ->with('message_warning', 'User tidak dapat dihapus.');
        }

        User::where('nip', '=', $nipx)->delete();
        return redirect()->route('user')
                    ->with('message_success', 'User ' . $user->nama . ' berhasil dihapus.');
    }

    public function doResetPassUser($nip) {
        $nipx = decrypt($nip);
        
        $user = User::where('nip', '=', $nipx)->first();
        $user->password = sha1(md5($user->nip));
        $user->save();
        return redirect()->route('user')
                    ->with('message_success', 'Password User ' . $user->nama . ' berhasil direset.');
    }

    public function changePassword() {
        $password_old = '';
        if (session('google_id')) {
            $user = User::where('nip', Auth::user()->nip)->first();
            if ($user != "") {
                $password_old = $user->password;
            }
        }

        return view('user.change_password', ['password_old' => $password_old]);
    }

    public function doChangePassword(Request $request) {
        $rules = array(
            'password_old' => 'required|min:5',
            'password' => 'required|min:5|confirmed',
            'password_confirmation' => 'required|min:5'
        );

        //change atribute name
        $attributeNames = array(
            'password_old' => 'Password Lama',
            'password' => 'Password Baru',
            'password_confirmation' => 'Konfirmasi Password Baru'
        );

        $messages = [
            'required' => ':attribute harus diisi.',
            'confirmed' => 'Password Baru tidak sama.',
            'min' => ':attribute minimal 5 karakter.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);


        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $user = User::where('nip', '=', Auth::user()->nip)->first();

            if ($user) {
                if (session('google_id')) {
                    if ($request->password_old == $user->password) {
                        $user->password = sha1(md5($_POST['password']));
                        $user->save();
                        return redirect()->route('profile')->with('message_success', 'Password berhasil diubah.');
                    } else {
                        return back()->with('message_warning', 'Password Lama salah.');
                    }
                } else {
                    if (sha1(md5($request->password_old)) == $user->password) {
                        $user->password = sha1(md5($_POST['password']));
                        $user->save();
                        return redirect()->route('profile')->with('message_success', 'Password berhasil diubah.');
                    } else {
                        return back()->with('message_warning', 'Password Lama salah.');
                    }
                }
            } else {
                return back()->with('message_warning', 'User tidak ditemukan.');
            }
        }
    }
    
    public function getNamaLikeNip(Request $request)
    {
        $nip = $request->term;

        $data = User::where('nip','like',$nip.'%')
                ->select(\DB::raw('nip as value, concat(nip," - ",nama) as label, nama'))->get();

        return response()->json($data);

    }

}
