<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\File;   

use Auth;

use Validator;

use App\Mail;

use Illuminate\Contracts\Filesystem\FileNotFoundException;

class FileController extends Controller
{
    protected $path = "files/";
    
    public function show($id)
    {
        $idx = decrypt($id);
        
        $file = File::where('id', $idx)->first();
        
        if($file == ""){
            return back()->with('message_warning','File tidak ditemukan.');
        }

        $filePath = $this->path.$file->filename;
        $name = $file->tipe.'_'.date('YmdHis').".".$file->ext;
        
        $headers = array(
                'Content-Type' => 'application/'.$file->ext,
                'Content-Disposition' => 'inline; filename="'.$name.'"'
            );
                
        try{  
           return response()->file($filePath, $headers);
        } catch (FileNotFoundException $ex) {
            abort(404, "File tidak ditemukan.");
        }
    	
    }
        
    public function remove($id)
    {
        $idx = decrypt($id);
        $file = File::where ('id',$idx)->first();
        $file->updated_by = Auth::user()->nip;
        $file->active = '0';
        $file->save();
        return back()->with('message_success','File '.ucwords($file->tipe).' berhasil dihapus.');
    }
    
    public function changeTipe($id)
    {
        $idx = decrypt($id);
        $file = File::where ('id',$idx)->first();
        $file->updated_by = Auth::user()->nip;
        $file->tipe = 'net';
        $file->save();
        return back()->with('message_success','File Draft berhasil dipilih menjadi Net.');
    }
    
    /**
     * show upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function uploadPendukung($id)
     {
          $idx = decrypt($id);
          
          $draft = Mail::where('id','=', $idx)->first();

          return view('draft.upload_pendukung',['draft' => $draft]);
     }
     
      /**
     * show process upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function doUploadPendukung(Request $request)
     {
      
          $rules = array(
                    'id' => 'required',
                    'file' => 'required|max:10000|mimes:pdf',
                    'keterangan' => 'required'
                );

          $messages = [
                        'file.required' => 'File belum dipilih.',
                        'keterangan.required' => 'Keterangan belum diisi',
                        'max' => 'Ukuran File maksimal 10 MB',
                        'mimes' => 'Format File harus pdf.'
                    ];
          

          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails())
          {
            return back()->withErrors($validator)->withInput();
          }
          
          $ext = $request->file->getClientOriginalExtension();
          $fileName = $request->id.'_pendukung_'.date('dmYHis').'_'. str_random(10).'.'.$request->file->getClientOriginalExtension();
          
          $upload = $request->file->move(public_path('files'), $fileName);
          
          if(!$upload){
                return back()
                            ->with('message_warning', 'Gagal upload file pendukung.');
            }
          
          
          $file = new File();
          $file->mail_id = $request->id;
          $file->tipe = 'pendukung';
          $file->filename = $fileName;
          $file->notes = $request->keterangan;
          $file->ext = $ext;
          $file->created_at = date('Y-m-d H:i:s');
          $file->created_by = Auth::user()->nip;
          $file->active = 1;
          try{
                $file->save();
                return redirect()->route('show-draft',[encrypt($request->id)])
                         ->with('message_success', 'File Pendukung berhasil diupload');
            } catch (QueryException $ex) {
                return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_warning', 'File Pendukung gagal diupload. Message:'.$ex->getMessage());
            }
          
    }
    
    /**
     * show upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function uploadDraft($id)
     {
          $idx = decrypt($id);
          
          $draft = Mail::where('id','=', $idx)->first();

          return view('draft.upload_draft',['draft' => $draft]);
     }


      /**
     * show process upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function doUploadDraft(Request $request)
     {
      
          $rules = array(
                    'id' => 'required',
                    'jenis' => 'required',
                    'keterangan' => 'required',
                    'file' => 'required|max:10000|mimes:docx,doc,xlsx,xls,pptx,ppt'
                );

          $messages = [
                        'jenis.required' => 'Jenis File harus diisi',
                        'keterangan.required' => 'Keterangan harus diisi',
                        'file.required' => 'File belum dipilih',
                        'max' => 'Ukuran File maksimal 10 MB',
                        'mimes' => 'Format File harus dokumen (docx,doc,xlsx,xls,pptx,ppt).'
                    ];
          

          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails())
          {
            return back()->withErrors($validator)->withInput();
          }
                   
          $ext = $request->file->getClientOriginalExtension();
          $fileName = $request->id.'_'.$request->jenis.'_'.date('dmYHis').'_'. str_random(10).'.'.$request->file->getClientOriginalExtension();
          
          $upload = $request->file->move(public_path('files'), $fileName);
          
          if(!$upload){
                return back()
                            ->with('message_warning', 'Gagal upload file Draft/Net.');
            }
          
          $file = new File();
          $file->mail_id = $request->id;
          $file->tipe = $request->jenis;
          $file->filename = $fileName;
          $file->ext = $ext;
          $file->notes = $request->keterangan;
          $file->created_at = date('Y-m-d H:i:s');
          $file->created_by = Auth::user()->nip;
          $file->active = 1;
          try{
                $file->save();
                return redirect()->route('show-draft',[encrypt($request->id)])
                         ->with('message_success', 'File Draft/Net berhasil diupload');
            } catch (QueryException $ex) {
                return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_warning', 'File Draft/Net gagal diupload. Message:'.$ex->getMessage());
            }
    }
    
    /**
     * show upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function uploadArsip($id)
     {
          $idx = decrypt($id);
          
          $arsip = Mail::where('id','=', $idx)->first();

          return view('arsip.upload_arsip',['arsip' => $arsip]);
     }
     
      /**
     * show process upload form.
     *
     * @return \Illuminate\Http\Response
     */
     public function doUploadArsip(Request $request)
     {
      
          $rules = array(
                    'id' => 'required',
                    'keterangan' => 'required',
                    'file' => 'required|max:10000|mimes:pdf'
                );

          $messages = [
                        'required' => ':attribute harus diisi.',
                        'max' => 'Ukuran File maksimal 10 MB',
                        'mimes' => 'Format File harus pdf.'
                    ];
          
          $attributeNames = [
                        'keterangan' => 'Keterangan',
                        'file' => 'File',
          ];
          
          $validator = Validator::make($request->all(), $rules, $messages);
          
          $validator->setAttributeNames($attributeNames);

          if ($validator->fails())
          {
            return back()->withErrors($validator)->withInput();
          }
                   
          $ext = $request->file->getClientOriginalExtension();
          $fileName = $request->id.'_'.$request->jenis.'_'.date('dmYHis').'_'. str_random(10).'.'.$request->file->getClientOriginalExtension();
          
          $upload = $request->file->move(public_path('files'), $fileName);
          
          if(!$upload){
                return back()
                            ->with('message_warning', 'Gagal upload file pendukung.');
            }
          
          $file = new File();
          $file->mail_id = $request->id;
          $file->tipe = 'arsip';
          $file->filename = $fileName;
          $file->ext = $ext;
          $file->notes = $request->keterangan;
          $file->created_at = date('Y-m-d H:i:s');
          $file->created_by = Auth::user()->nip;
          $file->active = 1;
          try{
                $file->save();
                return redirect()->route('show-arsip',[encrypt($request->id)])
                         ->with('message_success', 'File Arsip berhasil diupload');
            } catch (QueryException $ex) {
                return redirect()->route('show-arsip',[encrypt($request->id)])
                        ->with('message_warning', 'File Arsip gagal diupload. Message:'.$ex->getMessage());
            }
     }
}
