<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail;
use Auth;
use App\MailSent;
use App\MailArsip;
use Validator;
use \Illuminate\Database\QueryException;
use App\File;
use App\pejabat;



class ArsipController extends Controller
{
    public function index()
    {
        $year = Mail::selectRaw('distinct(year(tanggal)) as tahun')->whereNotNull('archived_at')->where('active','1')->orderBy('tahun','desc')->get();
        $data = [];
        foreach($year as $y){
            $p = $y->tahun;
            $sum = Mail::whereNotNull('archived_at')->whereYear('tanggal',$y->tahun)->where('active','1')->count();
            $data[] = ['tahun' => $p, 'sum' => $sum];
            
        }
        
        return view('arsip.index', ['data' => $data]);
    }
    
    public function arsip($id)
    {
        $thn = decrypt($id);
        
        // get mail created by user 
        $arsip5 = Mail::where('active','1')
                ->whereYear('tanggal',$thn)
                ->whereNotNull('archived_at')
                ->where('created_by',Auth::user()->nip)
                ->with('unit')
                ->with('pejabat')
                ->with('creator');
                //->orderBy('created_at','desc')
                //->get();
        
        // get mail sent to current user
        $arsip4 = Mail::where('active','1')
                ->whereYear('tanggal',$thn)
                ->whereNotNull('archived_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver',Auth::user()->nip);
                    $query->where('active','1');
                })
                ->with('unit')
                ->with('pejabat')
                ->with('creator');
                

        // get mail signed by user who has plt. or Plh.
        $arsip3 = Mail::where('active','1')
            ->whereNotNull('archived_at')
            ->whereYear('tanggal',$thn)
            ->whereHas('pejabat', function($query) {
                $query->where('nip',Auth::user()->nip);
                $query->where('active',1);
            })
            ->with('unit')
            ->with('pejabat')
            ->with('creator')
            //->union($arsip) -> restrict
            //->union($arsip4) -> restrict
            ->orderBy('created_at','desc') 
            ->get();
        
        $unit  = Pejabat::where('nip', Auth::user()->nip)
                    ->where('active', 1)
                    ->pluck('unit_id');
        
        //print_r($unit); exit();
        // get mail who has plt. or Plh. unit
        if(count($unit) > 0){
            $arsip2 = Mail::where('active','1')
            ->whereNotNull('archived_at')
            ->whereYear('tanggal',$thn)
            ->where(function ($query) use ($unit) {
                    for ($i = 0; $i < count($unit); $i++){
                        $query->Where('unit_id', 'like',  $unit[$i] .'%');
                    }             
             })  
            ->with('unit')
            ->with('pejabat')
            ->with('creator');
        } else {
            $arsip2 = Mail::where('active','99')
            ->whereNotNull('archived_at')
            ->whereYear('tanggal',$thn)
            ->where(function ($query) use ($unit) {
                    for ($i = 0; $i < count($unit); $i++){
                        $query->Where('unit_id', 'like',  $unit[$i] .'%');
                    }             
             })  
            ->with('unit')
            ->with('pejabat')
            ->with('creator');
        }
        
             //print_r($arsip2); exit();
        // get mail rahasia where unit like user unit and creator/konseptor
        $arsip1 = Mail::where('active','1')
            ->whereYear('tanggal',$thn)
            ->whereNotNull('archived_at')
            ->where('unit_id','like', Auth::user()->unit_id.'%')
            ->where('keamanan','rahasia') 
            ->where('created_by',Auth::user()->nip)
            ->with('unit')
            ->with('pejabat')
            ->with('creator');
                
        // get mail biasa where unit like user unit
        $arsip = Mail::where('active','1')
            ->whereYear('tanggal',$thn)
            ->whereNotNull('archived_at')
            ->where('unit_id','like', Auth::user()->unit_id.'%')
            ->where('keamanan','biasa')  // except secret mail
            ->with('unit')
            ->with('pejabat')
            ->with('creator')
            ->union($arsip1)
            ->union($arsip2)
            ->orderBy('tanggal','desc') 
            ->get();
        if(Auth::user()->role_id == 10){
            $arsip = Mail::whereNotNull('archived_at')->whereYear('tanggal',$thn)->where('active','1')->get();
        }
        
        return view('arsip.arsip_tahun', ['arsip' => $arsip, 'thn' => $thn]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $idx = decrypt($id);
        
        $mailsents = MailSent::where('mail_id',$idx)->where('active','1')->get();
        foreach($mailsents as $m){
            if($m->read_at == null AND $m->receiver == Auth::user()->nip AND $m->active = '1'){
                $mailsent = MailSent::where('id',$m->id)->first();
                $mailsent->read_at = date('Y-m-d H:i:s');
                $mailsent->save();
            }
        }
        
        $arsip = Mail::where('active','1')
                ->where('id',$idx)
                ->with('unit')
                ->with('pejabat')
                ->with(['mailSent' => function($query){
                    $query->where('active','1');
                }])
                ->with('file')
                ->with('latestMailSent')
                ->with('creator')
                ->with('mailArsip')
                ->first();
        
        
       
        return view('arsip.show', ['arsip' => $arsip]);
    }
    
    public function pindah($id)
    {
        $arsip = Mail::where('id',decrypt($id))->first();
        return view('arsip.pindah',['arsip' => $arsip]);
    }
    
    public function doPindah(Request $request)
    {
        $rules = [
            'id' => 'required',
            'tanggal' => 'required',
            'lokasi'   => 'required'
        ];
        
        $messages = [
            'required' => ':attribute harus diisi.'
        ];
        
        $attributeNames = [
            'tanggal' => 'Tanggal',
            'lokasi'   => 'Lokasi'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        } else {
            $arsip = new MailArsip();
            $arsip->mail_id = $request->id;
            $arsip->tanggal = date('Y-m-d', strtotime($request->tanggal));
            $arsip->lokasi = $request->lokasi;
            $arsip->created_at = date('Y-m-d H:i:s');
            $arsip->created_by = Auth::user()->nip;
            try{
                $arsip->save();
                return redirect()->route('show-arsip',[encrypt($request->id)])
                        ->with('message_success', 'Data pemindahan arsip berhasil disimpan.');
            } catch (QueryException $ex) {
                return redirect()->route('show-arsip',[encrypt($request->id)])
                        ->with('message_warning', 'Data pemindahan arsip gagal disimpan.');
            }
            
        }
    }
    

}
