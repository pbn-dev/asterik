<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Unit;

use Auth;

use Validator;

class UnitController extends Controller
{
    public function index()
    {
    	$unit = Unit::with('pejabat')->orderByRaw('left(id,2) asc')->orderByRaw('left(id,4) asc')->orderByRaw('left(id,6) asc')->get();
    	return view('unit.index', ['unit' => $unit]);
    }
    
    public function changeStatus($id)
    {
        $idx = decrypt($id);
        
        $unit = Unit::where('id',$idx)->first();
        
        if($unit->active == '1'){
            $unit->active = '0';
        } else {
            $unit->active = '1';
        }
        if(Auth::check()){
            $unit->updated_by = Auth::user()->nip;
        }              
        $unit->save();
        
        return redirect()->route('unit')->with('message_success','Status Unit berhasil diupdate.');
    }
    
    public function unitRekam() {
        $unit = Unit::orderByRaw('left(id,2) asc')->orderByRaw('left(id,4) asc')->orderByRaw('left(id,6) asc')->get();
        
        return view('unit.rekam',['unit' => $unit]);
    }
    
    public function doRekamUnit(Request $request) {

        $rules = array(
            'id' => 'required|numeric',
            'nama' => 'required',
            'jbtn' => 'required',
            'jbtn_short' => 'required'
        );

        //change atribute name
        $attributeNames = array(
            'id' => 'ID',
            'nama' => 'Nama Unit',
            'jbtn' => 'Nama Jabatan',
            'jbtn_short' => 'Singkatan Nama Jabatan'
        );

        $messages = [
            'required' => ':attribute harus diisi.',
            'numeric' => ':attribute harus dalam angka.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);


        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            
            $cek = Unit::where('id', $request->id)->count();
            if($cek > 0){
                return redirect()->route('unit')
                            ->with('message_warning', 'ID unit sudah ada.');
            }
            $unit = new Unit();
            $unit->id = $request->id;
            $unit->nama = $request->nama;
            $unit->jbtn = $request->jbtn;
            $unit->jbtn_short = $request->jbtn_short;            
            $unit->updated_by = Auth::user()->nip;
            $unit->active = '1';

            $unit->save();
            return redirect()->route('unit')
                            ->with('message_success', 'Penambahan unit ID:'.$request->id.' berhasil disimpan.');
        }
    }
    
    public function unitEdit($id) {

        $idx = decrypt($id);

        $unit= Unit::where('id', $idx)->first();
        $units = Unit::orderByRaw('left(id,2) asc')->orderByRaw('left(id,4) asc')->orderByRaw('left(id,6) asc')->get();
        return view('unit.edit', ['unit' => $unit, 'units' => $units]);
    }
    
    public function doEditUnit(Request $request) {

        $rules = array(
            'id' => 'required|numeric',
            'nama' => 'required',
            'jbtn' => 'required',
            'jbtn_short' => 'required'
        );

        //change atribute name
        $attributeNames = array(
            'id' => 'ID',
            'nama' => 'Nama Unit',
            'jbtn' => 'Nama Jabatan',
            'jbtn_short' => 'Singkatan Nama Jabatan'
        );

        $messages = [
            'required' => ':attribute harus diisi.',
            'numeric' => ':attribute harus dalam angka.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //change attribute name errors
        $validator->setAttributeNames($attributeNames);


        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            
            $unit = Unit::where('id', $request->id)->first();
            $unit->nama = $request->nama;
            $unit->jbtn = $request->jbtn;
            $unit->jbtn_short = $request->jbtn_short;            
            $unit->updated_by = Auth::user()->nip;

            $unit->save();
            return redirect()->route('unit')
                            ->with('message_success', 'Perubahan unit ID:'.$request->id.' berhasil disimpan.');
        }
    }
}
