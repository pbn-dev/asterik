<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\LoginLog;
use Exception;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    
    public function showLoginForm() 
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $rules = [
                    'username' => 'required',
                    'password' => 'required'
                ];

        $attributeNames = [
                            'username' => 'Username',
                            'password' => 'Password'
                        ];

        $messages = [
                        'required' => ':attribute harus diisi.'
                    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->setAttributeNames($attributeNames);


        if($validator->fails()){
            return back()->withErrors($validator);
        } else {
            $user = User::where('username',$request->username)->where('password', sha1(md5($request->password)))->first();
            $log = new LoginLog();
            $log->nip = $request->username;
            $log->ip = $this->getClientIp();

            if($user == ""){
                $log->success = '0';
                $log->save();
                return back()->with('message_warning','Username dan Password tidak valid');
            }
            
            $log->nip = $user->nip;
            
            if($user->active == 0){
                $log->success = '0';
                $log->save();
                return back()->with('message_warning','User dalam keadaan tidak aktif.');
            }
            
            $log->success = '1';
            $log->save();

            Auth::login($user);
            session()->put('foto', $this->getUrlFoto($request->username));

            return redirect()->route('beranda');
        }
    }

    public function logout()
    {
        
        Auth::logout();
        
        return redirect('login'); // redirect the user to the login screen
    }
    
    public function getUrlFoto($nip){
        $link_foto = asset('img/no_foto_default.png');
        if (!@getimagesize($link_foto)){
                $link_foto = asset('img/no_foto_default.png');
        } 
        
        return $link_foto; 
    }

    public function getClientIp(){
    $ipAddress = '';

        // Check for X-Forwarded-For headers and use those if found
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ('' !== trim($_SERVER['HTTP_X_FORWARDED_FOR']))) {
            $ipAddress = trim($_SERVER['HTTP_X_FORWARDED_FOR']);
        } else {
            if (isset($_SERVER['REMOTE_ADDR']) && ('' !== trim($_SERVER['REMOTE_ADDR']))) {
                $ipAddress = trim($_SERVER['REMOTE_ADDR']);
            }
        }

    return $ipAddress;
    }
    
}
