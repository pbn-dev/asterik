<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LoginLog;

class LogController extends Controller
{
    public function index()
    {
        $data = LoginLog::with('user')->orderBy('created_at','desc')->get();        
        return view('log.index',['data' => $data]);
    } 
}
