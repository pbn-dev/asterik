<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Unit;

use App\Pejabat;

use Validator;

use App\Mail;

use Auth;

use App\MailSent;

use App\File;

use App\User;

use \Illuminate\Database\QueryException;

use App\MailArsip;


class DraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        // get mail created by user 
        $draft = Mail::where('active','1')
                ->whereNull('archived_at')
                ->where('created_by',Auth::user()->nip)
                ->with('unit')
                ->with('pejabat')
                ->with('latestMailSent')
                ->with('creator');
                //->orderBy('created_at','desc')
                //->get();
        
        // get mail sent to current user
        $draft4 = Mail::where('active','1')
                ->whereNull('archived_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver',Auth::user()->nip);
                    $query->where('active','1');
                })
                ->with('unit')
                ->with('pejabat')
                ->with('latestMailSent')
                ->with('creator');
                
         // get mail where unit like user unit
            $draft2 = Mail::where('active','1')
                ->whereNull('archived_at')
                ->where('unit_id','like', Auth::user()->unit_id.'%')
                ->where('keamanan','biasa')  // except secret mail
                ->with('unit')
                ->with('pejabat')
                ->with('latestMailSent')
                ->with('creator');
            
            // get mail signed by user who has plt. or Plh.
            $draft3 = Mail::where('active','1')
                ->whereNull('archived_at')
                ->whereHas('pejabat', function($query) {
                    $query->where('nip',Auth::user()->nip);
                })
                ->with('unit')
                ->with('pejabat')
                ->with('latestMailSent')
                ->with('creator')
                ->union($draft)
                ->union($draft2)
                ->union($draft4)
                ->orderBy('created_at','desc') 
                ->get();

        return view('draft.index',['draft' => $draft3]);
    }
    
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = Unit::where('active','1')->get();
        $pejabat = Pejabat::where('active','1')->with('unit')->get();
        return view('draft.create',['unit' => $unit, 'pejabat' => $pejabat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nomor' => 'required',
            'hal'   => 'required',
            'sifat' => 'required',
            'keamanan' => 'required',
            'tujuan' => 'required',
            'unit_konseptor'  => 'required',
            'pejabat' => 'required',
            'keterangan' => 'required',
            'file' => 'required|max:10000|mimes:docx,doc,xlsx,xls,ppt,pptx'
        ];
        
        $attributeNames = [
            'nomor' => 'Nomor',
            'hal'   => 'Hal',
            'sifat' => 'Sifat',
            'keamanan' => 'Keamanan',
            'tujuan' => 'Tujuan',
            'unit_konseptor'  => 'Unit Konseptor',
            'pejabat'  => 'Penandatangan',
            'keterangan' => 'Keterangan',
            'file' => 'File'
        ];
        
        $messages = [
            'required' => ':attribute harus diisi.',
            'file.required' =>'File harus dipilih',
            'max' => 'Ukuran File maksimal 10 MB',
            'mimes' => 'Format File harus dokumen (docx, doc,xlsx, xls, ppt, pptx).'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $mail = new Mail();
            $mail->nomor = $request->nomor;
            $mail->hal = $request->hal;
            $mail->sifat = $request->sifat;
            $mail->keamanan = $request->keamanan;
            $mail->tujuan = $request->tujuan;
            $mail->unit_id = $request->unit_konseptor;
            $mail->notes = $request->keterangan;
            $mail->pejabat_id = $request->pejabat;
            $mail->active = '1';
            $mail->created_at = date('Y-m-d H:i:s');
            $mail->created_by = Auth::user()->nip;    
            
            $c_mail = Mail::whereYear('created_at',date('Y'))->count() + 1;
            $mail->ref = substr(date('Y'), 2).$c_mail;
            
            try{
                $mail->save();
            } catch (QueryException $ex) {
                return back()
                       ->withInput()
                       ->with('message_warning', 'Draft / Net gagal disimpan. Message: '.$ex->getMessage());
            }
            
            $ext = $request->file->getClientOriginalExtension();
            $fileName = $mail->id.'_draft_'.date('dmYHis').'_'. str_random(10).'.'.$ext;
            
            $upload = $request->file->move(public_path('files'), $fileName);
            
            if(!$upload){
                $mail->delete();
                return back()
                            ->with('message_warning', 'Gagal upload file. Draft / Net gagal disimpan');
            }
            
            $file = new File();
            $file->mail_id = $mail->id;
            $file->tipe = 'draft';
            $file->filename = $fileName;
            $file->ext = $ext;
            $file->notes = 'Draft awal';
            $file->created_at = date('Y-m-d H:i:s');
            $file->created_by = Auth::user()->nip;
            $file->active = 1;
            try{
                $file->save();
                return redirect()->route('draft')
                            ->with('message_success', 'Draft / Net berhasil disimpan');
            } catch (QueryException $ex) {
                $mail->delete();
                return back()
                            ->with('message_warning', 'File Draft / Net gagal disimpan. Message: '.$ex->getMessage());
            }
                        
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $idx = decrypt($id);
        
        $mailsents = MailSent::where('mail_id',$idx)->where('active','1')->get();
        foreach($mailsents as $m){
            if($m->read_at == null AND $m->receiver == Auth::user()->nip AND $m->active = '1'){
                $mailsent = MailSent::where('id',$m->id)->first();
                $mailsent->read_at = date('Y-m-d H:i:s');
                $mailsent->save();
            }
        }
        
        $draft = Mail::where('active','1')
                ->where('id',$idx)
                ->with('unit')
                ->with('pejabat')
                ->with(['mailSent' => function($query){
                    $query->where('active','1');
                }])
                ->with('file')
                ->with('latestMailSent')
                ->with('creator')
                ->first();
        
        
       
        return view('draft.show', ['draft' => $draft]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::where('active','1')->get();
        $pejabat = Pejabat::where('active','1')->with('unit')->get();
        $draft = Mail::where('id',decrypt($id))->first();
        return view('draft.edit',['unit' => $unit, 'pejabat' => $pejabat, 'draft' => $draft]);
    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'nomor' => 'required',
            'hal'   => 'required',
            'sifat' => 'required',
            'keamanan' => 'required',
            'tujuan' => 'required',
            'unit_konseptor'  => 'required',
            'pejabat' => 'required',
            'keterangan' => 'required'
        ];
        
        $attributeNames = [
            'nomor' => 'Nomor',
            'hal'   => 'Hal',
            'sifat' => 'Sifat',
            'keamanan' => 'Keamanan',
            'tujuan' => 'Tujuan',
            'unit_konseptor'  => 'Unit Konseptor',
            'pejabat'  => 'Penandatangan',
            'keterangan' => 'Keterangan'
        ];
        
        $messages = [
            'required' => ':attribute harus diisi.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            
            $mail = Mail::where('id', $request->id)->first();
            
            if($mail->approved_at != null){
                $num = explode(" ",$request->nomor);
                if(count($num) > 1){
                    return back()
                            ->with('message_warning','Isian nomor belum lengkap, tidak boleh mengandung spasi.')
                           ->withInput();
                }
            }
            
            $mail->nomor = $request->nomor;
            $mail->hal = $request->hal;
            $mail->sifat = $request->sifat;
            $mail->keamanan = $request->keamanan;
            $mail->tujuan = $request->tujuan;
            $mail->unit_id = $request->unit_konseptor;
            $mail->notes = $request->keterangan;
            $mail->pejabat_id = $request->pejabat;
            $mail->updated_at = date('Y-m-d H:i:s');
            $mail->updated_by = Auth::user()->nip;
            try{
                $mail->save();
                return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_success', 'Perubahan Draft / Net berhasil disimpan.');
            } catch (QueryException $ex) {
                return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_warning', 'Perubahan Draft / Net gagal disimpan. Message:'.$ex->getMessage());
            }
              
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mail = Mail::where('id', decrypt($id))->first();
        $mail->active = '0';
        $mail->updated_by = Auth::user()->nip;
        $mail->save();
        return redirect()->route('draft')
                    ->with('message_success', 'Draft / Net berhasil dihapus.');
    }
        
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function kirim($id)
    {
        $draft = Mail::where('id',decrypt($id))->first();
        return view('draft.kirim',['draft' => $draft]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function arsip($id)
    {
        $draft = Mail::where('id',decrypt($id))->first();
        return view('draft.arsip',['draft' => $draft]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doKirim(Request $request)
    {
        $rules = array(
            'id' => 'required',
            'tanggal' => 'required'
        );

        $messages = [
            'required' => ':attribute harus diisi'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
                
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $mail = Mail::where('id', $request->id)->first();
            $mail->sent_at = date('Y-m-d', strtotime($request->tanggal));
            $mail->updated_by = Auth::user()->nip;
            $mail->sent_who = Auth::user()->nip;
            $mail->save();
            return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_success', 'Data Pengiriman surat berhasil disimpan.');
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doArsip(Request $request)
    {
        $rules = array(
            'id' => 'required',
            'tanggal' => 'required',
            'lokasi' => 'required',
            'file' => 'required|max:10000|mimes:pdf'
        );

        $messages = [
            'required' => ':attribute harus diisi',
            'max' => 'Ukuran File maksimal 10 MB',
            'mimes' => 'Format File harus pdf.'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);
                
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $mail = Mail::where('id', $request->id)->first();
            $mail->archived_at = date('Y-m-d', strtotime($request->tanggal));
            $mail->updated_by = Auth::user()->nip;
            try{
                $mail->save();
            } catch (QueryException $ex) {
                return back()
                       ->withInput()
                       ->with('message_warning', 'Gagal menyimpan data pengarsipan. Message: '.$ex->getMessage());
            }
            
            $ext = $request->file->getClientOriginalExtension();
            $fileName = $mail->id.'_draft_'.date('dmYHis').'_'. str_random(10).'.'.$ext;
            
            $upload = $request->file->move(public_path('files'), $fileName);
            
            if(!$upload){
                $mail->archived_at = date('Y-m-d', strtotime($request->tanggal));
                $mail->archived_who = Auth::user()->nip;
                $mail->save();
                return back()
                            ->with('message_warning', 'Gagal upload file Arsip. Arsip gagal disimpan');
            }
            
            $file = new File();
            $file->mail_id = $mail->id;
            $file->tipe = 'arsip';
            $file->filename = $fileName;
            $file->ext = $ext;
            $file->notes = 'Arsip File';
            $file->created_at = date('Y-m-d H:i:s');
            $file->created_by = Auth::user()->nip;
            $file->active = 1;
            
            try{
                $file->save();
                
                $arsip = new MailArsip();
                $arsip->mail_id = $mail->id;
                $arsip->tanggal = $mail->archived_at;
                $arsip->lokasi = $request->lokasi;
                $arsip->created_at = date('Y-m-d H:i:s');
                $arsip->created_by = Auth::user()->nip;
                try{
                    $arsip->save();
                    return redirect()->route('draft')
                            ->with('message_success', 'Data arsip '.$mail->nomor.' berhasil disimpan.');
                } catch (QueryException $ex) {
                    return redirect()->route('show-draft',[encrypt($request->id)])
                            ->with('message_warning', 'Data arsip gagal disimpan.');
                }
            } catch (QueryException $ex) {
                $mail->archived_at = date('Y-m-d', strtotime($request->tanggal));
                $mail->archived_who = Auth::user()->nip;
                $mail->save();
                $file->delete();
                return back()
                            ->with('message_warning', 'Data arsip gagal disimpan. Message: '.$ex->getMessage());
            }
        }
    }

    
     /**
     * Approved the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approved($id)
    {
        $mail = Mail::where('id', decrypt($id))->first();
        $file = File::where('mail_id', decrypt($id))->where('tipe','net')->where('active','1')->count();
        if($file < 1){
            return redirect()->route('show-draft',[$id])
                    ->with('message_warning', 'Gagal menyetujui. File Net belum di-upload atau dipilih.');
        }
        $mail->approved_at = date('Y-m-d H:i:s');
        $mail->updated_by = Auth::user()->nip;
        $mail->approved_who = Auth::user()->nip;
        
        $mail->save();
        return redirect()->route('show-draft',[$id])
                    ->with('message_success', 'Draft / Net berhasil disetujui.');
    }
    
    /**
     * DisApproved the specified resource from storage when sent_at is null
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disApproved($id)
    {
        $mail = Mail::where('id', decrypt($id))->first();
        if($mail->sent_at != null){
            return redirect()->route('show-draft',[$id])
                    ->with('message_warning', 'Tidak dapat membatalkan persetujuan. Surat telah dikirim pada tanggal '.date('d-m-Y', strtotime($mail->sent_at)));
        }
        $mail->approved_at = NULL;
        $mail->approved_who = NULL;
        $mail->tanggal = NULL;
        $mail->tanggal_who = NULL;
        $mail->updated_by = Auth::user()->nip;
        $mail->save();
        return redirect()->route('show-draft',[$id])
                    ->with('message_success', 'Persetujuan Draft / Net berhasil dibatalkan.');
    }
    
       
    
    public function rekamTanggal($id)
    {
        
        $draft = Mail::where('id',decrypt($id))->first();
        return view('draft.rekam_tanggal',[ 'draft' => $draft]);
    }
    
    public function doRekamTanggal(Request $request)
    {
        $rules = [
            'id' => 'required',
            'tanggal' => 'required'
        ];
        
        $attributeNames = [
            'tanggal'   => 'Tanggal'
        ];
        
        $messages = [
            'required' => ':attribute harus diisi.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
                                
            $mail = Mail::where('id', $request->id)->first();
            $mail->tanggal = date('Y-m-d', strtotime($request->tanggal));
            $mail->updated_by = Auth::user()->nip;
            $mail->tanggal_who = Auth::user()->nip;
            $mail->save();
            return redirect()->route('edit-draft',[encrypt($request->id)])
                        ->with('message_success', 'Tanggal berhasil disimpan. Silahkan update nomor serta data lainnya jika terjadi perubahan.');
        }
    }
    
    public function draftCreatedByUser()
    {
        $draft = Mail::where('active','1')
                ->whereNull('archived_at')
                ->where('created_by',Auth::user()->nip)
                ->get();
        return view('draft.draft_created_by_user', ['draft' => $draft]);
    }
    
    public function draftNotReadByUser()
    {
        $draft = Mail::where('active','1')
                ->whereNull('archived_at')
                ->whereHas('mailSent', function($query) {
                    $query->where('receiver',Auth::user()->nip);
                    $query->whereNull('read_at');
                    $query->where('active','1');
                })->get();
        return view('draft.draft_not_read_by_user', ['draft' => $draft]);
    }
    
    public function draftNotForwardedByUser()
    {
        $mail = Mail::where('active','1')
            ->whereNull('archived_at')
            ->with('latestMailSent')->get();
        
        $draft = [];
        foreach($mail as $sp){
            if(isset($sp->latestMailSent->receiver)){
                if($sp->latestMailSent->receiver == Auth::user()->nip){
                    $draft[] = $sp;
                }
            } else {
                if($sp->created_by == Auth::user()->nip){
                    $draft[] = $sp;
                }
            }
        }
        
        return view('draft.draft_not_forwarded_by_user', ['draft' => $draft]);
    }
    
    public function draftNotApprovedByUser()
    {
        $draft = Mail::where('active','1')
            ->whereNull('archived_at')
            ->whereNull('approved_at')
            ->whereHas('pejabat', function($query) {
                $query->where('nip',Auth::user()->nip);
            })->get(); 
                
        return view('draft.draft_not_approved_by_user', ['draft' => $draft]);
    }
    
    public function draftNotGivenDateByUser()
    {
         $draft = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('approved_at')
            ->whereNull('tanggal')
            ->get();
                
        return view('draft.draft_not_given_date_by_user', ['draft' => $draft]);
    }
    
    public function draftNotSentByUser()
    {
         $draft = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('tanggal')
            ->whereNull('sent_at')
            ->get();
                
        return view('draft.draft_not_sent_by_user', ['draft' => $draft]);
    }
    
     public function draftNotArchivedByUser()
    {
         $draft = Mail::where('active','1')
            ->whereNull('archived_at')
            ->where('created_by', Auth::user()->nip)
            ->whereNotNull('sent_at')
            ->get();
                
        return view('draft.draft_not_archived_by_user', ['draft' => $draft]);
    }
    
}
