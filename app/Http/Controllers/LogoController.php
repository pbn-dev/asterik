<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class LogoController extends Controller
{
    public function index()
    {
        return view('logo.index');
    }
    
    public function upload(Request $request)
    {
        $rules = array(
                    'file' => 'required|max:5000|mimes:png',
                );

          $messages = [
                        'file.required' => 'Logo belum dipilih.',
                        'max' => 'Ukuran Logo maksimal 5 MB',
                        'mimes' => 'Format Logo harus png.'
                    ];
          

          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails())
          {
            return back()->withErrors($validator)->withInput();
          }
          
          $ext = $request->file->getClientOriginalExtension();
          $upload = $request->file->move(public_path('img'), 'logo.png');
          
          if(!$upload){
                return back()
                            ->with('message_warning', 'Gagal upload logo.');
            } else {
                return redirect()->route('logo')
                         ->with('message_success', 'Logo berhasil diupload');
            }
         
    }
    
    public function uploadFavicon(Request $request)
    {
        $rules = array(
                    'file' => 'required|max:1000|mimes:ico',
                );

          $messages = [
                        'file.required' => 'Favicon belum dipilih.',
                        'max' => 'Ukuran Favicon maksimal 1 MB',
                        'mimes' => 'Format Favicon harus ico.'
                    ];
          

          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails())
          {
            return back()->withErrors($validator)->withInput();
          }
          
          $ext = $request->file->getClientOriginalExtension();
          $upload = $request->file->move(public_path('img'), 'favicon.ico');
          
          if(!$upload){
                return back()
                            ->with('message_warning', 'Gagal upload favicon.');
            } else {
                return redirect()->route('logo')
                         ->with('message_success', 'Favicon berhasil diupload');
            }
         
    }
}
