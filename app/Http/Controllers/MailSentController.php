<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Mail;

use Auth;

use App\MailSent;

use Illuminate\Contracts\Filesystem\FileNotFoundException;

use App\User;

class MailSentController extends Controller
{
    /**
     * show kirim form.
     *
     * @return \Illuminate\Http\Response
     */
     public function teruskan($id)
     {
          $idx = decrypt($id);
          
          $draft = Mail::where('id','=', $idx)->first();
          
          $user = User::where('active','1')->where('nip','<>',Auth::user()->nip)->orderBy('unit_id','asc')->get();

          return view('draft.teruskan_draft',['draft' => $draft, 'user' => $user]);
     }
     
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function doTeruskan(Request $request)
    {
        $rules = [
            'id' => 'required',
            'receiver' => 'required',
            'keterangan'   => 'required'
        ];
        
        $attributeNames = [
            'receiver' => 'Penerima',
            'keterangan'   => 'Keterangan'
        ];
        
        $messages = [
            'required' => ':attribute harus diisi.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        $validator->setAttributeNames($attributeNames);
        
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {

            $mailSent = new MailSent();
            if (Auth::check()) {
                $mailSent->sender = Auth::user()->nip;
            }
            $mailSent->mail_id = $request->id;
            $mailSent->notes = $request->keterangan;
            $mailSent->receiver = $request->receiver;
            $mailSent->created_at = date('Y-m-d H:i:s');
            $mailSent->active = '1';
            try{
                $mailSent->save();
                return redirect()->route('show-draft',[encrypt($request->id)])
                            ->with('message_success', 'Draft / Net berhasil diteruskan.');
            } catch (QueryException $ex) {
                return redirect()->route('show-draft',[encrypt($request->id)])
                        ->with('message_warning', 'Draft / Net gagal diteruskan. Message:'.$ex->getMessage());
            }
            
            
        }
        
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMailSent($id)
    {
        $mail = MailSent::where('id', decrypt($id))->first();
        $mail->active = '0';
        $mail->save();
        return redirect()->route('show-draft',[encrypt($mail->mail_id)])
                    ->with('message_success', 'Kiriman Draft / Net berhasil dibatalkan.');
    }
    
}
