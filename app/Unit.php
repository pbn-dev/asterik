<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model {

    //
    protected $table = 'unit';

    public function user() {
        return $this->hasMany('App\User');
    }
    
    public function pejabat() {
        return $this->hasMany('App\Pejabat')->where('active',1);
    }
    
    public function mail() {
        return $this->hasMany('App\Mail');
    }


}
