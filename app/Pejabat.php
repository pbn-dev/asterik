<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pejabat extends Model
{
    protected $table = 'pejabat';
    public $timestamps = false;
    
    public function unit(){
        return $this->belongsTo('App\Unit');
    }
    
    public function mail() {
        return $this->hasMany('App\Mail');
    }
}
