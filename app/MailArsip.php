<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailArsip extends Model
{
    protected $table = 'mail_arsip';
    public $timestamps = false;
    
    public function mail(){
        return $this->belongsTo('App\Mail');
    }
    
    public function creator(){
        return $this->belongsTo('App\User','created_by','nip');
    }
}
