<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $table = 'mail';
    
    public function unit(){
        return $this->belongsTo('App\Unit');
    }
    
    public function pejabat(){
        return $this->belongsTo('App\Pejabat');
    }
    
    public function mailSent(){
        return $this->hasMany('App\MailSent');
    }
    
    public function mailArsip(){
        return $this->hasMany('App\MailArsip');
    }
    
    public function latestMailSent(){
        return $this->hasOne('App\MailSent')->where('active','1')->latest(); //last by created_at
    }
    
    public function creator(){
        return $this->belongsTo('App\User','created_by','nip');
    }
    
    public function file(){
        return $this->hasMany('App\File');
    }
}
