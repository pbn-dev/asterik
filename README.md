# ASTERIK #

Aplikasi SuraT kEluaR Internal Kantor (ASTERIK) adalah aplikasi surat untuk merekam data konsep surat keluar hingga pengarsipan.

### 1. Persyaratan Umum ###

Aplikasi ini menggunakan framework laravel 5.4.28, sehingga harus memenuhi spesifikasi server sebagai berikut:

* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

untuk informasi lebih lanjut terkait laravel klik di [sini](https://laravel.com/docs/5.4).

### 2. Instalasi server (Windows) ###

#### 2.1. Instalasi XAMPP ####
* Download XAMPP for Windows 5.6.31 di website [XAMPP](https://www.apachefriends.org/xampp-files/5.6.31/xampp-win32-5.6.31-0-VC11-installer.exe).
* Lakukan instalasi XAMPP di drive "C:". Petunjuk instalasi dapat dilihat di [sini](http://www.wikihow.com/Install-XAMPP-for-Windows).
* Setelah instalasi jalankan xampp.
* Tambahkan path "C:\xampp\mysql\bin" dan "C:\xampp\php" di system variables:
	- Klik kanan pada MY Computer Icon dan klik Properties atau masuk ke "Control Panel \ System and Security \ System".
	- Pilih "Advanced System Settings" dan pilih "Advance" Tab.
	- Sekarang pilih "Environment Variable" pilihan dan pilih "Path" dari "System Variables" dan klik pada tombol "Edit".
	- Sekarang tambahkan kedua set path diatas pada variable value (setiap path dipisahkan tanda ';').
	- Setelah set path Klik Ok dan Apply.
* Ubah password mysql seperti langkah di [sini](https://veerasundar.com/blog/2009/01/how-to-change-the-root-password-for-mysql-in-xampp/) (abaikan langkah 8).
* Simpan password mysql yang telah Anda Ubah untuk digunakan ke langkah selanjutnya.
* Ubahlah value `$cfg['Servers'][$i]['auth_type']` menjadi 'cookie'.

#### 2.2. Instalasi GIT dan Composer ####
* Download Git di [sini](https://git-scm.com/downloads) lalu install.
* Tambahkan path "C:\Program Files\Git\cmd" di system variables.
* download Composer di [sini](https://getcomposer.org/Composer-Setup.exe) lalu install.


### 3. Download dan Import Database Asterik ###
* Download database asterik terbaru di [sini](https://bitbucket.org/pbn-dev/asterik/downloads/).
* Ketikan "localhost/phpmyadmin" di browser, misalnya mozilla firefox.
* isikan user: root dan password dengan password mysql.
* Buat database baru 'asterik_pbn' lalu impor db_asterik_pbn.sql.

### 4. Download dan Setting Aplikasi Asterik ###
* Buka command promt (cmd) lalu ketikan `"cd c:\xampp\htdocs\"` dan tekan enter.
* ketikan `"git clone https://fromjayakarta@bitbucket.org/pbn-dev/asterik.git"` lalu enter.
* ketikan kembali `"cd c:\xampp\htdocs\asterik"` dan enter.
* Ketikan `"php artisan key:generate"` dan enter untuk menggenerate key aplikasi.
* Buka file .env, lalu ubah `"DB_PASSWORD"` value dengan password mysql.
* Ketikan `"localhost/asterik"` pada browser server dan akan muncul halaman login.
* ketikan admin pada username dan password lalu klik login, maka Anda Akan login dengan user "admin".
* Ubahlah password "admin" dengan melakukan klik tombol "profile" dari Foto sebelah kanan atas. Lalu klik ganti password.
* Klik menu Admin > user, tambahkan user untuk masing-masing pegawai.
* Klik menu Admin > pejabat, tambahkan data pejabat sebagai penandatangan surat.
* Mungkin Anda perlu untuk menonaktifkan user 'admin' dengan melakukan klik label 'Aktif' berwarna hijau pada kolom status, sehingg tidak muncul saat pencarian user.


### 5. Bekerja dengan Aplikasi Asterik ###
* ketikan `ipconfig` pada cmd di komputer server, dan temukan IP ADDRESS untuk diinfokan ke user lainnya..
* setiap komputer klien yang terhubung ke komputer server dalam jaringan LAN, dapat mengakses Aplikasi Asterik dengan mengetikan "IP_ADDRESS_SERVER/asterik" pada browser.
* User melakukan login menggunakan username dan password yang telah diberikan.
* "Halaman Beranda" menampilkan dashboard penyelesaian surat untuk masing-masing user, yang akan otomatis merefresh setiap lima detik.
* User membuat draft/net baru di menu Draft / Net, dengan melakukan klik tombol 'Buat Baru". Silahkan baca info ketentuan dalam pembuatan draft /net di bagian bawah "Halaman Rekam Draft/Net".
* Halaman Draft/Net akan menampilkan data surat/Net:
	- Dibuat oleh Anda.
	- Diterima dari penerusan user lainnya.
	- unit konseptor surat sama dengan unit user atau dibawah Anda (kecuali untuk surat yang bersifat rahasia).
	- Anda sebagai penandatangan surat, perlu menyetujui draft/net yang diajukan.
* Setelah Anda membuat draft/net, silahkan teruskan secara berjenjang kepada pejabat penandatangan untuk mendapatkan persetujuan.
* Setelah disetujui, silahkan berikan tanggal dan nomor surat tersebut.
* Rekam data pengiriman surat, jika surat telah dikirim.
* Surat yang telah diarsipkan, dicatat data pengarsipannya.
* Surat yang telah direkam data pengarsipan, akan menjadi arsip, yang diakses melalui menu "Arsip".

### 6. Update 5 September 2017 ###
* Menambahkan Fitur upload logo dan favicon aplikasi
* Menambahkan penamaan aplikasi

#### 6.1. Petunjuk Update ####
* Buka command promt (cmd) lalu ketikan `"cd c:\xampp\htdocs\"` dan tekan enter.
* Jika Ada ingin memberikan nama aplikasi menjadi "sisuka", maka ketikan `"git clone https://fromjayakarta@bitbucket.org/pbn-dev/asterik.git sisuka"` lalu enter. Pastikan sebelumnya tidak ada folder dengan nama "sisuka" dalam htdocs.
* Setelah clone berhasil, ketikan kembali `"cd c:\xampp\htdocs\asterik"` dan enter.
* Ketikan `"php artisan key:generate"` dan enter untuk menggenerate key aplikasi.
* Buka file .env, lalu ubah `"DB_PASSWORD"` value dengan password mysql dan `"APP_NAME"` value dengan SISUKA (atau nama aplikasi yang diinginkan).
* Copy file-file yang ada pada folder "public/files/" di aplikasi yang lama, lalu paste ke folder "public/files/ pada aplikasi yang baru, misalnya dari folder "/asterik/public/files/" ke folder "/sisuka/public/files/".
* Login ke aplikasi kewenangan admin menggunakan url baru (misalnya: "IP_ADDRESS_SERVER/sisuka"), lalu klik menu Admin > Logo dan Favicon. Silahkan upload file Logo dan Favicon pada form yang tersedia. Misalnya, download file logo [disini](https://bitbucket.org/pbn-dev/asterik/downloads/kppn.png), lalu upload pada form logo.

#### 6.2. Petunjuk Menyederhanakan URL Aplikasi ####
* Sebelumnya, untuk masuk ke dalam aplikasi perlu mengetikan URL "IP_ADDRESSS_SERVER/sisuka". Lalu Anda akan dialihkan ke halaman ""IP_ADDRESSS_SERVER/sisuka/public/login".
* Anda dapat masuk ke dalam aplikasi hanya dengan mengetikan IP_ADDRESS_SERVER pada url, dengan melakukan langkah-langkah berikut:
	- buka file "C:xampp\apache\conf\httpd.conf". (sesuaikan dengan direktori instalasi xampp Anda).
	- Cari baris berisi `DocumentRoot "C:/xampp/htdocs"` dan ganti dengan `DocumentRoot "C:/xampp/htdocs/sisuka/public"`.
	- Cari baris berisi `<Directory "C:/xampp/htdocs">` dan ganti dengan `DocumentRoot "<Directory "C:/xampp/htdocs/sisuka/public">`.
	- Simpan perubahan lalu restart apache.
	
### 7. Update 27 November 2017 ###
* Remove bug pada halaman arsip karena tidak dapat menampilkan keamanan surat
* Petunjuk
	- Backup dahulu folder `"c:\xampp\htdocs\asterik"` dengan melakukan copy ke direktori lainnya.
	- Buka command promt (cmd) lalu ketikan `"cd c:\xampp\htdocs\asterik"` dan tekan enter.
	- ketik `"git pull"` dan tekan enter.

### 8. Update 9 Agustus 2019 ###
* Menambahkan Rekam/Ubah/Hapus referensi unit pada admin
* Menambahkan filter pada daftar 'Arsip'. Arsip hanya dapat dilihat oleh Pegawai dan Plt/Plh. Pejabat Penandatangan pada unit berkenaan. Khusus surat yang bersifat rahasia hanya pejabat unit dan pegawai pembuat surat pada unit berkenaan yang dapat melihat.
* Menampilkan informasi role pejabat pada beranda

-- Creator : Slamet Darmawan (fromjayakarta@gmail.com) ---